# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.RootHandler
# Author:  Mats Blomdahl
# Version: 2013-02-27

import json
import logging

from google.appengine.api import taskqueue

from google.appengine.ext import ndb
from google.appengine.ext import db
from google.appengine.datastore.datastore_query import Cursor

import ecore.cloudstorage as gcs
import ecore.formatting as formatting

import mstat_model.transactions as transaction_model

import mstat_constants as constants

import mstat_generic_handler as generic_handler


_DEFAULT_PAGESIZE = 25

_DEFAULT_SOURCING_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_SOURCING_QUEUE)
_DEFAULT_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_PROCESSING_QUEUE)

_VALID_RAW_DATA_KINDS = ['CapitexRawDataEntry', 'SfdRawDataEntry', 'ScbRawDataEntry', 'SfRawDataEntry',
                         'LfRawDataEntry']


class RootHandler(generic_handler.CommonTaskQueueInterface):
    u"""Handlers for performing manual updates on the GAE Datastore.

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_worker/handlers.yaml` config).
    """

    # Interface: /backend/worker/source_entities_for_transaction_data_backfill
    def source_entities_for_transaction_data_backfill(self):
        u"""Recursively queries NDB for target entity keys and dispatches backfill jobs to the GAE TaskQueue.

        Raises:
            AssertionError on invalid request parameters for `kind`, `batch_size`, `refresh_only` or `to_composite`.
        """

        import ecore.model.transactions as ecore_transaction_model

        kind = self.request.get('kind', default_value=None)
        kind_cls = eval('transaction_model.%s' % kind)
        assert issubclass(kind_cls, ecore_transaction_model.TransactionData) and hasattr(kind_cls, 'backfill')
        kind_cls_read_policy = kind_cls.get_default_export_consistency()

        batch_size = int(self.request.get('batch_size', default_value=_DEFAULT_PAGESIZE))
        assert batch_size > 0

        countdown = int(self.request.get('countdown', default_value='60'))
        assert countdown >= 0

        refresh_only = self.request.get('refresh_only', default_value='false').lower()
        assert refresh_only in ['true', 'false']

        to_composite = self.request.get('to_composite', default_value='true').lower()
        assert to_composite in ['true', 'false']
        if to_composite == 'true':
            assert not isinstance(kind_cls, transaction_model.TransactionCompositeEntry)

        write_queue = self.request.get('write_queue', default_value=_DEFAULT_QUEUE.name)

        cursor = Cursor(urlsafe=self.request.get('cursor', default_value=None))
        query = kind_cls.query()
        entity_keys, next_cursor, has_more = query.fetch_page(batch_size,
                                                              keys_only=True,
                                                              batch_size=batch_size,
                                                              start_cursor=cursor,
                                                              read_policy=kind_cls_read_policy)
        if len(entity_keys):
            _DEFAULT_QUEUE.add(taskqueue.Task(
                url='/backend/worker/dispatch_entities_for_transaction_data_backfill',
                params={
                    'ndb_keys_urlsafe': formatting.tq_serialize([key.urlsafe() for key in entity_keys]),
                    'kind': kind,
                    'refresh_only': refresh_only,
                    'to_composite': to_composite,
                    'write_queue': write_queue
                },
                countdown=countdown
            ))

        if has_more:
            batch_size = 1000
            _DEFAULT_SOURCING_QUEUE.add(taskqueue.Task(
                url='/backend/worker/source_entities_for_transaction_data_backfill',
                params={
                    'cursor': next_cursor.urlsafe(),
                    'kind': kind,
                    'refresh_only': refresh_only,
                    'to_composite': to_composite,
                    'write_queue': write_queue,
                    'batch_size': batch_size,
                    'countdown': countdown
                }
            ))

    # Interface: /backend/worker/dispatch_entities_for_transaction_data_backfill
    def dispatch_entities_for_transaction_data_backfill(self):
        u"""Performs backfill, rewrite and, optionally, `to-composite` export for transaction data entities referenced
            by `ndb_keys_urlsafe`.

        Raises:
            AssertionError on invalid request parameters for `kind`, `ndb_keys_urlsafe`, `to_composite` or
                `refresh_only`.
        """

        ndb_keys_urlsafe = formatting.tq_deserialize(self.request.get('ndb_keys_urlsafe', default_value=None))
        write_queue = taskqueue.Queue(self.request.get('write_queue', default_value=None))
        base_params = {'kind': self.request.get('kind', default_value=None),
                       'to_composite': self.request.get('to_composite', default_value=None),
                       'refresh_only': self.request.get('refresh_only', default_value=None)}
        for val in base_params.values():
            assert val is not None

        for key in ndb_keys_urlsafe:
            base_params['ndb_keys_urlsafe'] = formatting.tq_serialize([key])
            while True:
                try:
                    write_queue.add(taskqueue.Task(
                        url='/backend/worker/process_entities_for_transaction_data_backfill',
                        params=base_params
                    ))
                    break
                except Exception as e:
                    logging.error('[RootHandler.dispatch_entities_for_transaction_data_backfill] Raised Exception: %r'
                                  % e.message)

    # Interface: /backend/worker/process_entities_for_transaction_data_backfill
    def process_entities_for_transaction_data_backfill(self):
        u"""Performs backfill, rewrite and, optionally, `to-composite` export for transaction data entities referenced
            by `ndb_keys_urlsafe`.

        Raises:
            AssertionError on invalid request parameters for `kind`, `ndb_keys_urlsafe` or `to_composite`.
        """

        kind = self.request.get('kind', default_value=None)
        kind_cls = eval('transaction_model.%s' % kind)  # TODO(mats.blomdahl@gmail.com): Shame! Clean-up.
        assert kind == kind_cls.__name__
        kind_cls_read_policy = kind_cls.get_default_export_consistency()

        ndb_keys_urlsafe = self.request.get('ndb_keys_urlsafe', default_value=None)
        assert ndb_keys_urlsafe is not None

        entity_keys = [ndb.Key(urlsafe=key) for key in formatting.tq_deserialize(ndb_keys_urlsafe)]

        refresh_only = self.request.get('refresh_only') == 'true'

        to_composite = self.request.get('to_composite') == 'true'
        if to_composite:
            assert not isinstance(kind_cls, transaction_model.TransactionCompositeEntry)

        # TODO(mats.blomdahl@gmail.com): Shame! Use ctx.
        futures, entities = [], [entity for entity in ndb.get_multi(entity_keys, deadline=3600,
                                                                    read_policy=kind_cls_read_policy)
                                 if entity is not None]

        for i in range(len(entities)):
            logging.debug('[RootHandler.process_entities_for_transaction_data_backfill] Updating entity %r of kind %r.'
                          % (entities[i].key.id(), entities[i].key.kind()))

            try:
                entities[i].backfill()
            except AssertionError as e:
                logging.error('Aborting... (\'%s\')' % e.message)
                return

            if to_composite and hasattr(kind_cls, 'to_transaction_composite'):
                try:
                    composite, future = entities[i].to_transaction_composite(dry_run=refresh_only,
                                                                             delegate_future_mgnt=True)
                    if isinstance(future, (list, tuple)):
                        futures.extend(future)
                    else:
                        futures.append(future)
                except AssertionError as e:  # TODO(mats.blomdahl@gmail.com): Shame! Replace with model-specific err.
                    logging.error('[RootHandler.process_entities_for_transaction_data_backfill] \'to_transaction_'
                                  'composite\' raised AssertionError: \'%s\'' % e.message)

        if not refresh_only:
            futures.extend(ndb.put_multi_async(entities,
                                               deadline=3600))  # TODO(mats.blomdahl@gmail.com): Shame! Use ctx.

        ndb.Future.wait_all(futures)
        for future in futures:
            future.get_result()

    # Interface: /backend/worker/source_raw_data_for_transaction_composite
    def source_raw_data_for_transaction_composite(self):
        u"""Recursively queries NDB for target entity keys and dispatches `raw-data-to-composite` export jobs to the GAE
            TaskQueue.

        Raises:
            AssertionError on invalid request parameters for `kind` or `batch_size`.
        """

        kind = self.request.get('kind', default_value=None)
        kind_cls = eval('transaction_model.%s' % kind)  # TODO(mats.blomdahl@gmail.com): Shame! Clean-up.
        assert kind == kind_cls.__name__ and kind in _VALID_RAW_DATA_KINDS

        batch_size = int(self.request.get('batch_size', default_value=_DEFAULT_PAGESIZE))
        assert batch_size > 0

        cursor = Cursor(urlsafe=self.request.get('cursor', default_value=None))
        entity_keys, next_cursor, has_more = kind_cls.query().fetch_page(batch_size,
                                                                         keys_only=True,
                                                                         batch_size=batch_size,
                                                                         start_cursor=cursor,
                                                                         read_policy=db.STRONG_CONSISTENCY)
        if len(entity_keys):
            _DEFAULT_QUEUE.add(taskqueue.Task(
                url='/backend/worker/process_raw_data_to_transaction_composite',
                params={
                    'ndb_keys_urlsafe': formatting.tq_serialize([key.urlsafe() for key in entity_keys]),
                    'kind': kind
                }
            ))

        if has_more:
            _DEFAULT_SOURCING_QUEUE.add(taskqueue.Task(
                url='/backend/worker/source_raw_data_for_transaction_composite',
                params={'cursor': next_cursor.urlsafe(), 'kind': kind}
            ))

    # Interface: /backend/worker/process_raw_data_to_transaction_composite
    def process_raw_data_to_transaction_composite(self):
        u"""Performs backfill, rewrite and `to-composite` export for raw data entities referenced by `ndb_keys_urlsafe`.

        Raises:
            AssertionError on invalid request parameters for `kind` or `ndb_keys_urlsafe`.
        """

        kind = self.request.get('kind', default_value=None)
        kind_cls = eval('transaction_model.%s' % kind)  # TODO(mats.blomdahl@gmail.com): Shame! Clean-up.
        assert kind == kind_cls.__name__ and kind in _VALID_RAW_DATA_KINDS

        ndb_keys_urlsafe = self.request.get('ndb_keys_urlsafe', default_value=None)
        assert ndb_keys_urlsafe is not None
        entity_keys = [ndb.Key(urlsafe=key) for key in formatting.tq_deserialize(ndb_keys_urlsafe)]

        entities, futures = ndb.get_multi(entity_keys, read_policy=db.STRONG_CONSISTENCY), []
        for i in range(len(entities)):
            logging.debug('[RootHandler.process_raw_data_to_transaction_composite] Updating entity %r of kind %r...'
                          % (entities[i].key.id(), entities[i].key.kind()))

            entities[i] = kind_cls.backfill(entities[i])

            try:
                futures.append(entities[i].to_transaction_composite())
            except AssertionError as e:  # TODO(mats.blomdahl@gmail.com): Shame! Replace with model-specific err.
                logging.error('[RootHandler.process_raw_data_to_transaction_composite] \'to_transaction_composite\' '
                              'raised AssertionError: \'%s\'' % e.message)

        futures.extend(ndb.put_multi_async(entities, deadline=3600))  # TODO(mats.blomdahl@gmail.com): Shame! Use ctx.

        ndb.Future.wait_all(futures)
        for future in futures:
            future.get_result()

    # Interface: /backend/worker/gs_resource_delete
    def gs_resource_delete(self):
        u"""Performs deletion of a Cloud Storage resource."""

        gs_uris = self.request.get('gs_uris', default_value=None)
        assert gs_uris is not None
        for uri in json.loads(gs_uris):
            try:
                gcs.delete(uri.split('gs:/')[-1])
            except gcs.NotFoundError as e:
                logging.error('[RootHandler.gs_resource_delete] Raised gcs.NotFoundError: \'%s\'' % e.message)

    # Interface: /backend/worker/pull_task_delete
    def pull_task_delete(self):
        u"""Performs deletion of a GAE TaskQueue `pull task`.

        Raises:
            AssertionError on invalid request parameters for `queue_name` or `task_names`.
        """

        queue_name = self.request.get('queue_name', default_value=None)
        assert queue_name is not None

        task_names_json = self.request.get('task_names', default_value=None)
        assert task_names_json is not None

        taskqueue.Queue(queue_name).delete_tasks_by_name([str(name) for name in json.loads(task_names_json)])
