# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.bq_sync
# Author:  Mats Blomdahl
# Version: 2014-02-01

u"""mstat_worker.bq_sync module

Usage:
    Invoke URLs at ``/backend/worker/bq_sync/<cmd>`` using the GAE TaskQueue or Cron Service.
"""

from bq_sync_handler import BqSyncHandler
