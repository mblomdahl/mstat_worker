# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.bq_sync_process.BqSyncHandler
# Author:  Mats Blomdahl
# Version: 2014-04-03

import datetime
import json
import logging
import time
import pprint

# TODO(mats.blomdahl@gmail.com): Deprecated / replace with client library.
from google.appengine.api import files
try:
    files.gs
except AttributeError:
    import gs
    files.gs = gs

from google.appengine.api import taskqueue

from google.appengine.ext.ndb import stats
from google.appengine.ext import ndb

import ecore.utils as ecore_utils
import ecore.formatting as formatting
import ecore.bigquery as bigquery
import ecore.model.properties as ecore_properties

import ecore.constants as ecore_constants

import mstat_constants as constants

import mstat_generic_handler as generic_handler

import bq_multiprocessing_lock
BqMultiprocessingLock = bq_multiprocessing_lock.BqMultiprocessingLock


_DEFAULT_BATCH_SIZE = 250

_DEFAULT_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_PROCESSING_QUEUE)
_DEFAULT_SOURCING_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_SOURCING_QUEUE)
_BQ_INGESTIONS_QUEUE = taskqueue.Queue(constants.WORKER_BQ_INGESTIONS_QUEUE)
_DATASTORE_WRITE_QUEUE = taskqueue.Queue(constants.WORKER_SYNC_PROCESSING_QUEUE)
_PULL_TASK_DELETION_QUEUE = taskqueue.Queue(constants.WORKER_PULL_TASK_DELETION_QUEUE)
_GS_RESOURCE_DELETION_QUEUE = taskqueue.Queue(constants.WORKER_GS_RESOURCE_DELETION_QUEUE)

_DIRTY_STATE_RESETS_PULL_QUEUE = taskqueue.Queue(constants.WORKER_DIRTY_STATE_RESETS_PULL_QUEUE)

_DEFAULT_OUTPUT_FORMAT = ecore_constants.BQ_JSON_FORMAT

_DEFAULT_TARGET_KINDS = ['TransactionCompositeEntry',
                         'SfRawDataEntry', 'LfRawDataEntry', 'CapitexRawDataEntry', 'SfdRawDataEntry',
                         'ScbRawDataEntry']

_BASE_DATE_PROPERTY = ecore_properties._UxDateProperty

_GS_MAX_WRITE_RETRIES = 100

_BQ_SYNC_T0 = datetime.date(2005, 1, 1)
_BQ_SYNC_PROCESS_ID_PREFIX = 'BQ_SYNC_PROCESS_%s_%s'


class _TargetDatasetModifiedException(Exception):
    u"""Raised when underlying data is changed during the export job. Should be cause for a rollback."""


def _resolve_kind(kind):
    import mstat_model.transactions as transaction_model

    if kind and kind not in _DEFAULT_TARGET_KINDS:
        raise AssertionError('Undefined Datastore kind \'%s\'.' % kind)

    # TODO(mats.blomdahl@gmail.com): Shame! Clean-up.
    try:
        kind_cls = eval('transaction_model.%s' % kind)
    except AttributeError:
        raise AssertionError('Cannot resolve Datastore kind \'%s\'.' % kind)
    finally:
        assert kind == kind_cls.__name__
        return kind_cls


def _resolve_kind_and_contract_date_property(kind):
    kind_cls = _resolve_kind(kind)
    return kind_cls, kind_cls.get_contract_date_property()


def _get_kind_min_contract_date(kind_cls):
    contract_date_property = kind_cls.get_contract_date_property()
    lower_date_lim = contract_date_property._get_for_dict(kind_cls.query().order(contract_date_property).get())
    logging.debug('[bq_sync_handler._get_kind_min_contract_date] \'lower_date_lim\': %s' % lower_date_lim.isoformat())
    return lower_date_lim


def _write_to_gs(gs_bucket, payload, bq_output_format):
    if bq_output_format == ecore_constants.BQ_CSV_FORMAT:
        gs_media_mime_type = 'text/csv'
    else:  # bq_output_format == ecore_constants.BQ_JSON_FORMAT:
        gs_media_mime_type = 'application/json'

    gs_media = u'/gs/%s/%s-output.%s' % (gs_bucket, datetime.datetime.today().isoformat(),
                                         gs_media_mime_type.split('/')[-1])
    logging.debug(
        '[BqSyncHandler._write_to_gs] Writing %i kB payload to gs_media %r...' % (len(payload) / 1024, gs_media))

    write_path = files.gs.create(gs_media, mime_type=gs_media_mime_type, acl=ecore_constants.GS_DEFAULT_ACL)
    for i in range(_GS_MAX_WRITE_RETRIES):
        try:
            # Write to the file.
            with files.open(write_path, 'a') as fp:
                fp.write(payload)

            # Finalize the file; so it's readable in Google Cloud Storage.
            files.finalize(write_path)
            break
        except files.FileNotOpenedError as e:
            logging.error('[bq_sync_handler._write_to_gs] Cloud Storage write attempt %i raised FileNotOpenedError: %r.'
                          % (i + 1, e.message))
            time.sleep(10)
            if i == (_GS_MAX_WRITE_RETRIES - 1):
                raise files.FileNotOpenedError(e.message)

    return gs_media.replace('/gs/', 'gs://')


class BqSyncHandler(generic_handler.CommonTaskQueueInterface):
    u"""Handlers synchronizing Datastore entities with their corresponding BigQuery resources.

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_worker/handlers.yaml` config).
    """

    # Interface: /backend/worker/bq_sync/locate_bq_synchronization_targets[?kind=<kind|all>[&start_date=<start_date>
    #     [&end_date=<end_date>[&bq_output_format=<CSV|NEWLINE_DELIMITED_JSON>[&refresh=<sys_dirty|all>]]]]]
    def locate_bq_synchronization_targets(self):
        u"""Recursively defines BigQuery `table sync jobs` and dispatches ``preprocess_destination_bq_table_resource``
            tasks to the TaskQueue API.

        Raises:
            AssertionError on invalid/undefined ``kind`` argument.
            ValueError on invalid ``bq_output_format`` or ``refresh`` arguments.
        """

        _PARAMS = ['start_date', 'end_date', 'bq_output_format', 'refresh']

        kind = self.request.get('kind', default_value='all')
        if kind == 'all':
            task_cfg = ecore_utils.dict_update_on_truthy({}, dict(zip(_PARAMS, map(self.request.get, _PARAMS))))
            for kind_name in _DEFAULT_TARGET_KINDS:
                task_cfg['kind'] = kind_name
                _DEFAULT_SOURCING_QUEUE.add(
                    taskqueue.Task(url='/backend/worker/bq_sync/locate_bq_synchronization_targets', params=task_cfg))
        else:
            kind_cls, kind_contract_date_property = _resolve_kind_and_contract_date_property(kind)

            try:
                start_date = formatting.coerce_to_dt(self.request.get('start_date'))
            except (ValueError, AttributeError):
                logging.debug(
                    '[BqSyncHandler.locate_bq_synchronization_targets] \'start_date\' missing, defaulting to t(0).')
                start_date = formatting.coerce_to_dt(_BQ_SYNC_T0)  # _get_kind_min_contract_date(kind_cls)

            try:
                end_date = formatting.coerce_to_dt(self.request.get('end_date'))
            except (ValueError, AttributeError):
                logging.debug(
                    '[BqSyncHandler.locate_bq_synchronization_targets] \'end_date\' missing, defaulting to today...')
                end_date = datetime.datetime.today()

            if issubclass(kind_contract_date_property.__class__, _BASE_DATE_PROPERTY):
                start_date, end_date = formatting.coerce_to_date(start_date), formatting.coerce_to_date(end_date)

            bq_output_format = self.request.get('bq_output_format') or _DEFAULT_OUTPUT_FORMAT
            if bq_output_format == ecore_constants.BQ_JSON_FORMAT:
                output_version = ecore_constants.BQ_JSON
            elif bq_output_format == ecore_constants.BQ_CSV_FORMAT:
                output_version = ecore_constants.BQ_CSV
            else:
                raise ValueError('Invalid BigQuery output format \'%s\'.' % bq_output_format)

            refresh = self.request.get('refresh', default_value='sys_dirty').lower()
            if refresh not in ['sys_dirty', 'all']:
                raise ValueError('Invalid refresh option \'%s\'.' % refresh)
            elif refresh == 'sys_dirty':
                query = kind_cls.query(ndb.AND(kind_cls.sys_dirty == True, kind_contract_date_property >= start_date,
                                       kind_contract_date_property < end_date)).order(kind_contract_date_property)
                entity = query.get()
                if entity is None:
                    return  # No `dirty` entities to sync -> nothing to do.

                qualified_table_id, table_date_range = entity.get_bq_table_id(include_dt_range=True,
                                                                              version=output_version)
                logging.info('[BqSyncHandler.locate_bq_synchronization_targets] Found dirty BQ table \'%s\' (%s--%s).'
                             % (qualified_table_id, table_date_range[0].isoformat(), table_date_range[1].isoformat()))

            else:  # Implicit ``refresh == 'all'``.
                if not start_date < end_date:
                    return  # Sync interval exhausted -> nothing to do.

                qualified_table_id, table_date_range = \
                    kind_cls._get_bq_table_id_by_contract_date(contract_date=start_date, include_dt_range=True,
                                                               version=output_version)
                logging.info('[BqSyncHandler.locate_bq_synchronization_targets] Found target BQ table \'%s\' (%s--%s).'
                             % (qualified_table_id, table_date_range[0].isoformat(), table_date_range[1].isoformat()))

            assert qualified_table_id and len(table_date_range)

            _DEFAULT_QUEUE.add(taskqueue.Task(  # Rewrite table containing the `dirty` entity.
                url='/backend/worker/bq_sync/preprocess_destination_bq_table_resource',
                params={
                    'kind': kind,
                    'qualified_table_id': qualified_table_id,
                    'lower_date_limit': table_date_range[0].isoformat(),
                    'upper_date_limit': table_date_range[1].isoformat(),
                    'bq_output_format': bq_output_format,
                    'output_version': output_version,
                    'refresh': refresh
                }
            ))

            logging.debug('[BqSyncHandler.locate_bq_synchronization_targets] Querying NDB for entities of kind \'%s\' '
                          'in range %s--%s.' % (kind, table_date_range[1].isoformat(), end_date.isoformat()))

            # Proceed with querying for `dirty` entities in range [`table-upper-date-lim`, ``end_date``).
            _DEFAULT_SOURCING_QUEUE.add(taskqueue.Task(
                url='/backend/worker/bq_sync/locate_bq_synchronization_targets',
                params={
                    'kind': kind,
                    'start_date': table_date_range[1].isoformat(),
                    'end_date': end_date.isoformat(),
                    'bq_output_format': bq_output_format,
                    'refresh': refresh
                }
            ))

    # Interface: /backend/worker/bq_sync/preallocate_bq_table_resources[?kind=<kind|all>[&target_date=<target_date>
    #     [&bq_output_format=<CSV|NEWLINE_DELIMITED_JSON>]]]
    def preallocate_bq_table_resources(self):
        u"""Queues pre-allocation of a BQ table for the previous day.

        This allocation makes sure each passing day receives it's own dedicated – and possibly empty – BQ table
            resource, whether any dirty entities exists or not.

        To avoid duplicate export ops, this hook should be called _before_ ``locate_bq_synchronization_targets`` on any
            given date and invoked only once.

        Raises:
            AssertionError on invalid/undefined ``kind`` argument.
            ValueError on invalid ``bq_output_format`` argument.
        """

        yesterday = datetime.datetime.today() - datetime.timedelta(days=1)
        target_date = formatting.coerce_to_date(self.request.get('target_date', default_value=yesterday))

        kind = self.request.get('kind', default_value='all')
        if kind == 'all':
            for kind_name in _DEFAULT_TARGET_KINDS:
                _DEFAULT_QUEUE.add(taskqueue.Task(url='/backend/worker/bq_sync/preallocate_bq_table_resources',
                                                  params={'kind': kind_name, 'target_date': target_date.isoformat()}))
        else:
            kind_cls, kind_contract_date_property = _resolve_kind_and_contract_date_property(kind)

            bq_output_format = self.request.get('bq_output_format', default_value=_DEFAULT_OUTPUT_FORMAT)
            if bq_output_format == ecore_constants.BQ_JSON_FORMAT:
                output_version = ecore_constants.BQ_JSON
            elif bq_output_format == ecore_constants.BQ_CSV_FORMAT:
                output_version = ecore_constants.BQ_CSV
            else:
                raise ValueError('Invalid BigQuery output format \'%s\'.' % bq_output_format)

            logging.debug(
                '[BqSyncHandler.preallocate_bq_table_resources] kind: \'%s\' / kind_count: %i / target_date: \'%s\''
                % (kind, stats.KindStat.query(stats.KindStat.kind_name == kind).get().count, target_date.isoformat()))

            qualified_table_id, table_date_range = kind_cls._get_bq_table_id_by_contract_date(contract_date=target_date,
                                                                                              include_dt_range=True,
                                                                                              version=output_version)
            # Rewrite table for ``target_date``.
            _DEFAULT_QUEUE.add(taskqueue.Task(
                url='/backend/worker/bq_sync/preprocess_destination_bq_table_resource',
                params={
                    'kind': kind_cls.__name__,
                    'qualified_table_id': qualified_table_id,
                    'lower_date_limit': table_date_range[0].isoformat(),
                    'upper_date_limit': table_date_range[1].isoformat(),
                    'bq_output_format': bq_output_format,
                    'output_version': output_version
                }
            ))

    # Interface: /backend/worker/bq_sync/preprocess_destination_bq_table_resource?kind=<kind>&qualified_table_id=
    #     <qualified_table_id>&bq_output_format=<CSV|NEWLINE_DELIMITED_JSON>&output_version=bq_<json|csv>_output
    #     &lower_date_limit=<lower_date_limit>&upper_date_limit=<upper_date_limit>
    def preprocess_destination_bq_table_resource(self):
        u"""Makes sure the target BQ resource is readily available and dispatches a `Datastore-subset-to-BigQuery-table`
            export job.

        Raises:
            AssertionError on missing request parameters for ``kind``, ``qualified_table_id`` or ``output_version``.
            ValueError on invalid/malformed ``bq_output_format``, ``lower_date_limit`` or ``upper_date_limit``
                arguments.
        """

        kind_cls = _resolve_kind(self.request.get('kind', default_value=None))

        qualified_table_id = self.request.get('qualified_table_id', default_value=None)
        assert qualified_table_id is not None

        bq_output_format = self.request.get('bq_output_format', default_value=None)
        assert bq_output_format is not None

        output_version = self.request.get('output_version', default_value=None)
        assert output_version is not None

        lower_date_limit = formatting.coerce_to_date(self.request.get('lower_date_limit'))
        upper_date_limit = formatting.coerce_to_date(self.request.get('upper_date_limit'))

        multiprocessing_lock = BqMultiprocessingLock(qualified_table_id).acquire()
        if not multiprocessing_lock:
            return  # Cancels BQ table resource pre-processing.

        # Dispatches 1 job per 2 days to collect-format-submit source data to CloudStorage prior to BQ ingestion job.
        date_intervals = [(lower.isoformat(), upper.isoformat())
                          for lower, upper in formatting.date_range_sub_intervals(lower_date_limit, upper_date_limit, 2,
                                                                                  output_date_type=datetime.date)]
        task_data = {
            'kind': kind_cls.__name__,
            'sync_process_id': _BQ_SYNC_PROCESS_ID_PREFIX % (qualified_table_id, multiprocessing_lock),
            'sync_initiated_dt': datetime.datetime.today(),
            'qualified_table_id': qualified_table_id,
            'date_intervals': date_intervals,
            'gs_ingestion_uris': [],
            'gs_bucket': kind_cls.get_gs_bucket(constants.GS_BQ_INGESTION_JOBS_BUCKET, output_version, prefix=False),
            'bq_output_format': bq_output_format,
            'output_version': output_version,
            'sys_dirty_entities_count': 0,
            'processed_entities_count': 0,
            'dirty_state_update_jobs_submitted': 0,
            'bq_sync_success': None,
            'multiprocessing_lock': multiprocessing_lock
        }
        logging.info('[BqSyncHandler.preprocess_destination_bq_table_resource] Starting BigQuery sync process '
                     '(\'task_cfg\': \'%s\').' % pprint.pformat(task_data))

        _DEFAULT_QUEUE.add(taskqueue.Task(url='/backend/worker/bq_sync/process_transaction_data_to_gs_ingestion_bucket',
                                          payload=formatting.tq_serialize(task_data),
                                          countdown=5))

    # Interface: /backend/worker/bq_sync/process_transaction_data_to_gs_ingestion_bucket
    def process_transaction_data_to_gs_ingestion_bucket(self):
        u"""Recursively queries NDB for target entities and writes the CSV output to CloudStorage/GS.

        After initial export formatting, entities tagged with the ``sys_dirty`` flag are submitted to the App Engine
            TaskQueue API to have their flags reset once successful BigQuery ingestion has been confirmed.
        """

        task_data = formatting.tq_deserialize(self.request.body)
        kind_cls, kind_contract_date_property = _resolve_kind_and_contract_date_property(task_data['kind'])

        sync_process_id, sync_initiated_dt = task_data['sync_process_id'], task_data['sync_initiated_dt']
        qualified_table_id = task_data['qualified_table_id']
        try:
            multiprocessing_lock = BqMultiprocessingLock(qualified_table_id,
                                                         lock=task_data['multiprocessing_lock']).verify()
        except bq_multiprocessing_lock.LockCompromisedError as e:  # Fail silently.
            logging.error(
                '[BqSyncHandler.process_transaction_data_to_gs_ingestion_bucket] Raised LockCompromisedError: \'%s\''
                % e.message)
            _DEFAULT_QUEUE.add(taskqueue.Task(url='/backend/worker/bq_sync/post_bq_sync_reset',
                                              payload=formatting.tq_serialize(task_data)))
            return

        lower_date_limit, upper_date_limit = task_data['date_intervals'].pop(0)
        if issubclass(kind_contract_date_property.__class__, _BASE_DATE_PROPERTY):
            lower_date_limit, upper_date_limit = \
                formatting.coerce_to_date(lower_date_limit), formatting.coerce_to_date(upper_date_limit)
        else:
            lower_date_limit, upper_date_limit = \
                formatting.coerce_to_dt(lower_date_limit), formatting.coerce_to_dt(upper_date_limit)

        logging.debug('[BqSyncHandler.process_transaction_data_to_gs_ingestion_bucket] Querying datastore for entities '
                      'of kind \'%s\' in range %s--%s, for insertion into \'%s\' (multiprocessing_lock: \'%s\')...'
                      % (task_data['kind'], lower_date_limit.isoformat(), upper_date_limit.isoformat(),
                         qualified_table_id, multiprocessing_lock))
        query = \
            kind_cls.query(ndb.AND(kind_contract_date_property >= lower_date_limit,
                                   kind_contract_date_property < upper_date_limit)).order(kind_contract_date_property)
        subset_count_future = query.count_async()

        sys_dirty_entities = []

        def _to_bq_export_format(entity):
            u"""Translates entity properties into standardized CSV or JSON output.

            Args:
                entity: A ``TransactionDataEntry`` instance retrieved from Datastore (required).

            Returns:
                A row of serialized output data.

            Raises:
                _TargetDatasetModifiedException when Datastore entity has a been modified since the sync job started.
            """

            if entity.sys_modified > task_data['sync_initiated_dt']:
                raise _TargetDatasetModifiedException('Identified entity with \'sys_modified\' %s (initiated sync '
                                                      'process at %s).' % (entity.sys_modified.isoformat(),
                                                                           sync_initiated_dt.isoformat()))
            elif entity.sys_dirty:
                sys_dirty_entities.append((entity.key.urlsafe(), entity.sys_modified))

            if task_data['bq_output_format'] == ecore_constants.BQ_CSV_FORMAT:
                bq_csv_output = entity.to_csv(output_version=task_data['output_version'],
                                              dialect=ecore_constants.BQ_CSV_WRITER_DIALECT)
                return bq_csv_output[1].encode(ecore_constants.DEFAULT_ENCODING)

            else:  # Implicit ``task_data['bq_output_format'] == ecore_constants.BQ_JSON_FORMAT``.
                return '%s\n' % entity.to_json(output_version=task_data['output_version'], datastore_refs=False,
                                               serialize=True)

        # TODO(mats.blomdahl@gmail.com): Shame! Use ctx.
        query_result = query.map(_to_bq_export_format, deadline=3600, batch_size=_DEFAULT_BATCH_SIZE,
                                 read_policy=kind_cls.get_default_export_consistency())

        logging.debug('[BqSyncHandler.process_transaction_data_to_gs_ingestion_bucket] len(query_result): %i / query'
                      '.count_async(): %i / len(sys_dirty_entities): %i' % (len(query_result),
                                                                            subset_count_future.get_result(),
                                                                            len(sys_dirty_entities)))
        if any(query_result):
            task_data['processed_entities_count'] += len(query_result)
            task_data['sys_dirty_entities_count'] += len(sys_dirty_entities)
            logging.debug('[BqSyncHandler.process_transaction_data_to_gs_ingestion_bucket] query_result[0]: \n\t\'%s\''
                          % query_result[0])

            logging.info('[BqSyncHandler.process_transaction_data_to_gs_ingestion_bucket] Retrieved %i entities for '
                         'transfer to Cloud Storage ingestion bucket \'%s\'.' % (len(query_result),
                                                                                 task_data['gs_bucket']))
            task_data['gs_ingestion_uris'].append(_write_to_gs(task_data['gs_bucket'], ''.join(query_result),
                                                               task_data['bq_output_format']))

            if any(sys_dirty_entities):  # Submit update targets to TaskQueue.
                def _to_dirty_state_resets_queue(reset_entities):
                    _DIRTY_STATE_RESETS_PULL_QUEUE.add(taskqueue.Task(method='PULL',
                                                                      tag=sync_process_id,
                                                                      payload=formatting.tq_serialize({
                                                                          'kind': task_data['kind'],
                                                                          'reset_entities': reset_entities,
                                                                          'sync_process_id': sync_process_id,
                                                                          'sync_initiated_dt': sync_initiated_dt,
                                                                          'qualified_table_id': qualified_table_id
                                                                      })))

                while any(sys_dirty_entities):
                    _to_dirty_state_resets_queue(sys_dirty_entities[:50])
                    sys_dirty_entities = sys_dirty_entities[50:]
                    task_data['dirty_state_update_jobs_submitted'] += 1

        else:
            logging.info('[BqSyncHandler.process_transaction_data_to_gs_ingestion_bucket] Retrieved 0 entities for '
                         'transfer to Cloud Storage.')

        if any(task_data['date_intervals']):  # Continue recursion.
            _DEFAULT_QUEUE.add(taskqueue.Task(
                url='/backend/worker/bq_sync/process_transaction_data_to_gs_ingestion_bucket',
                payload=formatting.tq_serialize(task_data)
            ))
        else:  # Proceed to finalize BQ ingestion job.
            _BQ_INGESTIONS_QUEUE.add(taskqueue.Task(
                url='/backend/worker/bq_sync/finalize_bq_synchronization',
                payload=formatting.tq_serialize(task_data)
            ))

    # Interface: /backend/worker/bq_sync/finalize_bq_synchronization
    def finalize_bq_synchronization(self):
        u"""Finalizes the `Datastore-subset-to-BigQuery-table` export job initiated from
            ``preprocess_destination_bq_table_resource``.

        This step submits a BQ table resource ingestion job, referencing the data files generated from the proceeding
            ``process_transaction_data_to_gs_ingestion_bucket`` invocations (i.e. the ``gs_ingestion_uris`` argument).

        Raises:
            NotImplementedError when there's no data available for the ingestion job and the export process is using the
                ``BQ_CSV`` import format.
            NotImplementedError on BigQuery error responses.
        """

        task_data = formatting.tq_deserialize(self.request.body)

        sync_process_id, sync_initiated_dt = task_data['sync_process_id'], task_data['sync_initiated_dt']

        kind_cls, qualified_table_id = _resolve_kind(task_data['kind']), task_data['qualified_table_id']

        try:
            multiprocessing_lock = BqMultiprocessingLock(qualified_table_id,
                                                         lock=task_data['multiprocessing_lock']).verify()
        except bq_multiprocessing_lock.LockCompromisedError as e:  # Fail silently.
            logging.error('[BqSyncHandler.finalize_bq_synchronization] Raised LockCompromisedError: \'%s\'' % e.message)
            _DEFAULT_QUEUE.add(taskqueue.Task(url='/backend/worker/bq_sync/post_bq_sync_reset',
                                              payload=formatting.tq_serialize(task_data)))
            return

        bq_output_format, output_version = task_data['bq_output_format'], task_data['output_version']
        if bq_output_format == ecore_constants.BQ_CSV_FORMAT:
            output_format = ecore_constants.CSV_OUTPUT
        else:  # i.e. bq_output_format == ecore_constants.BQ_JSON_FORMAT
            output_format = ecore_constants.JSON_OUTPUT

        processed_entities_count, sys_dirty_entities_count = \
            task_data['processed_entities_count'], task_data['sys_dirty_entities_count']

        gs_ingestion_uris = task_data['gs_ingestion_uris']
        if processed_entities_count == 0:
            if bq_output_format == ecore_constants.BQ_JSON_FORMAT:  # Write a dummy `null-row`.
                gs_ingestion_uris.append(_write_to_gs(task_data['gs_bucket'], '{}', bq_output_format))
                task_data['gs_ingestion_uris'] = gs_ingestion_uris
            else:
                # TODO(mats.blomdahl@gmail.com): Implementation.
                raise NotImplementedError('No support for creating empty tables using CSV format.')

        logging.info('[BqSyncHandler.finalize_bq_synchronization] Finalizing BigQuery sync process \'%s\' (sync'
                     '_initiated_dt: %s, total_entities_processed: %i, sys_dirty_entities_processed: %i).'
                     % (sync_process_id, sync_initiated_dt, processed_entities_count, sys_dirty_entities_count))

        project_id, dataset_id, table_id = formatting.bq_id_to_pdt(qualified_table_id)
        logging.debug('[BqSyncHandler.finalize_bq_synchronization] Ingesting URIs \'%s\' into project \'%s\', dataset '
                      '\'%s\' and table \'%s\' (multiprocessing_lock: \'%s\')...' % (pprint.pformat(gs_ingestion_uris),
                                                                                     project_id, dataset_id, table_id,
                                                                                     multiprocessing_lock))

        entity_schema = kind_cls.get_kind_schema(output_format=output_format,
                                                 output_version=output_version).to_bq_api_format()
        logging.debug(
            '[BqSyncHandler.finalize_bq_synchronization] \'entity_schema\': \n\n\t%s' % pprint.pformat(entity_schema))

        load_job = bigquery.InsertJob(project_id=project_id,
                                      job_config=bigquery.resources.JobConfiguration(
                                          origin_api_call=ecore_constants.BQ_INSERT_JOB,
                                          project_id=project_id,
                                          load={
                                              'source_uris': gs_ingestion_uris,
                                              'schema': entity_schema,
                                              'source_format': bq_output_format,
                                              'destination_table': {'dataset_id': dataset_id, 'table_id': table_id},
                                              'write_disposition': ecore_constants.BQ_WRITE_TRUNCATE_DISPOSITION,
                                              'max_bad_records': 0
                                          }))

        load_job_id, load_job_response, fallback_delay = \
            load_job.get_response_job_id(), load_job.get_json_response(), 8
        while True:
            if load_job_response['data'][-1]['status']['state'] == 'DONE':
                # Removes the ``schema`` config, as it will cause more important sections of the response to overflow.
                load_job_response['data'][-1]['configuration']['load'].pop('schema')
                break
            else:
                fallback_delay = fallback_delay * 2 if fallback_delay < 128 else 256
                logging.debug('[BqSyncHandler.finalize_bq_synchronization] Waiting %i sec.. (load_job_response: \'%s\')'
                              % (fallback_delay, pprint.pformat(load_job_response)))
                time.sleep(fallback_delay)

            logging.debug('[BqSyncHandler.finalize_bq_synchronization] Querying job ID \'%s\'...' % load_job_id)
            load_job_response = bigquery.GetJob(project_id=project_id, job_id=load_job_id).get_json_response()

        if 'errorResult' not in load_job_response['data'][-1]['status']:  # The job succeeded.
            task_data['bq_sync_success'] = True
            logging.info('[BqSyncHandler.finalize_bq_synchronization] Successfully finalized load job \'%s\' (load_job'
                         '_response: \'%s\')' % (load_job_id, pprint.pformat(load_job_response)))
            _DEFAULT_QUEUE.add(taskqueue.Task(url='/backend/worker/bq_sync/post_bq_sync_reset',
                                              payload=formatting.tq_serialize(task_data)))
        else:  # The job failed.
            task_data['bq_sync_success'] = False
            logging.error('[BqSyncHandler.finalize_bq_synchronization] load_job_response: \'%s\''
                          % pprint.pformat(load_job_response['data'][-1]))
            logging.error('[BqSyncHandler.finalize_bq_synchronization] Raw response: \'%s\''
                          % pprint.pformat(load_job.get_raw_response()[0].to_dict()))
            # TODO(mats.blomdahl@gmail.com): Implementation.
            raise NotImplementedError('We don\'t yet have any satisfactory handling for ingestion failures. :-/')

    # Interface: /backend/worker/bq_sync/post_bq_sync_reset
    def post_bq_sync_reset(self):
        u"""Performs clean-up and post-processing once the ``finalize_bq_synchronization`` ingestion has completed.

        In detail: Cloud Storage resources from the BigQuery ingestion are deleted, synchronized entities have their
            ``sys_dirty`` flag reset, the target table `multi-processing lock` is released and `pull tasks` are pulled
            from the TaskQueue.
        """

        task_data = formatting.tq_deserialize(self.request.body)

        if any(task_data['gs_ingestion_uris']):
            gs_uri = task_data['gs_ingestion_uris'].pop(0)
            _GS_RESOURCE_DELETION_QUEUE.add(taskqueue.Task(url='/backend/worker/gs_resource_delete',
                                                           params={'gs_uris': json.dumps([gs_uri])}))
            logging.debug('[BqSyncHandler.post_bq_sync_reset] Dispatched deletion job for URI \'%s\' (%i left to go)...'
                          % (gs_uri, len(task_data['gs_ingestion_uris'])))

        if task_data['dirty_state_update_jobs_submitted'] > 0:
            update_tasks = _DIRTY_STATE_RESETS_PULL_QUEUE.lease_tasks_by_tag(1800, 4, tag=task_data['sync_process_id'])
            for update_task in update_tasks:
                task_data['dirty_state_update_jobs_submitted'] -= 1

                if task_data['bq_sync_success'] is True:
                    countdown = task_data['dirty_state_update_jobs_submitted'] * 2
                    _DATASTORE_WRITE_QUEUE.add(taskqueue.Task(url='/backend/worker/bq_sync/process_dirty_state_update',
                                                              payload=update_task.payload,
                                                              countdown=countdown))
                    logging.debug(
                        '[BqSyncHandler.post_bq_sync_reset] Pulled update task \'%s\' from BigQuery sync process \'%s\''
                        '. %i update tasks remaining...' % (update_task.name, task_data['sync_process_id'],
                                                            task_data['dirty_state_update_jobs_submitted']))

                _PULL_TASK_DELETION_QUEUE.add(taskqueue.Task(url='/backend/worker/pull_task_delete',
                                                             params={'queue_name': _DIRTY_STATE_RESETS_PULL_QUEUE.name,
                                                                     'task_names': json.dumps([update_task.name])},
                                                             countdown=1800))
                if not task_data['bq_sync_success']:
                    logging.debug(
                        '[BqSyncHandler.post_bq_sync_reset] Dispatched deletion job for invalidated update task \'%s\' '
                        '(originating from failed BigQuery sync process \'%s\' at %s).'
                        % (update_task.name, task_data['sync_process_id'], task_data['sync_initiated_dt'].isoformat()))

        if any(task_data['gs_ingestion_uris']) or task_data['dirty_state_update_jobs_submitted'] > 0:
            # Continue recursion.
            _DEFAULT_QUEUE.add(taskqueue.Task(url='/backend/worker/bq_sync/post_bq_sync_reset',
                                              payload=formatting.tq_serialize(task_data)))
        else:
            # Reset completed.
            qualified_table_id = task_data['qualified_table_id']
            try:
                multiprocessing_lock = BqMultiprocessingLock(qualified_table_id,
                                                             lock=task_data['multiprocessing_lock']).release()
                logging.info('[BqSyncHandler.post_bq_sync_reset] Completed reset after BigQuery sync op on \'%s\' (sync'
                             '_process_id: \'%s\' / multiprocessing_lock: \'%s\').' % (qualified_table_id,
                                                                                       task_data['sync_process_id'],
                                                                                       multiprocessing_lock))
            except ValueError as e:
                logging.error('[BqSyncHandler.post_bq_sync_reset] Raised ValueError: \'%s\'' % e.message)

    # Interface: /backend/worker/bq_sync/process_dirty_state_update
    def process_dirty_state_update(self):
        u"""Resets entity ``sys_dirty`` flags to ``False`` after successful BigQuery sync.

        Raises:
            AssertionError on Datastore write errors.
        """

        task_data = formatting.tq_deserialize(self.request.body)
        kind_cls, qualified_table_id = _resolve_kind(task_data['kind']), task_data['qualified_table_id']
        sync_process_id, sync_initiated_dt = task_data['sync_process_id'], task_data['sync_initiated_dt']

        entity_keys = dict([(ndb.Key(urlsafe=urlsafe_key), sys_modified)
                            for urlsafe_key, sys_modified in task_data['reset_entities']])

        logging.info(
            '[BqSyncHandler.process_dirty_state_update] Resetting \'sys_dirty\' state for %i entities of kind \'%s\', '
            'following BigQuery sync to \'%s\' (sync_process_id: \'%s\' / sync_initiated_dt: %s).'
            % (len(entity_keys), task_data['kind'], qualified_table_id, sync_process_id, sync_initiated_dt.isoformat()))

        entities = ndb.get_multi(entity_keys.keys(), deadline=3600)  # TODO(mats.blomdahl@gmail.com): Shame! Use ctx.

        def _reset_dirty_state(target_entity):
            if target_entity.sys_modified == entity_keys[target_entity.key]:
                target_entity.sys_dirty = False
                return target_entity
            else:
                return None

        keys_updated = ndb.put_multi([entity for entity in map(_reset_dirty_state, entities) if entity is not None],
                                     deadline=3600)  # TODO(mats.blomdahl@gmail.com): Shame! Use ctx.
        for key in keys_updated:
            assert key is not None


