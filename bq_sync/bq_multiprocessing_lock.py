# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.bq_sync_process.BqMultiprocessingLock
# Author:  Mats Blomdahl
# Version: 2014-04-06

import logging
import datetime

from google.appengine.api import memcache

import ecore.formatting as formatting


class LockCompromisedError(Exception):
    u"""Raised when a lock value is changed externally (without first being released)."""


class LockUpdateNetworkError(Exception):
    u"""Raised on network errors or Memcache failures."""


class BqMultiprocessingLock(object):
    u"""Simple multi-processing lock using the App Engine Memcache."""

    _MEM_LOCK_PREFIX = 'BQ_SYNC_LOCK_%s'
    _MEM_LOCK_TIMEOUT = 3600 * 4
    _MEM_LOCK_COOLDOWN = 600

    def __init__(self, qualified_table_id, lock=None):
        u"""Initializes the lock representation for a BigQuery table resource.

        Args:
            qualified_table_id: Fully-qualified BigQuery table ID (required).
            lock: Lock value, if already acquired.

        Raises:
            ValueError on invalid ``qualified_table_id`` input value.
        """

        if not qualified_table_id:
            raise ValueError('Required \'qualified_table_id\' argument missing/invalid.')
        self._qualified_table_id = qualified_table_id
        self._lock_value = lock
        self._mem_key = self._MEM_LOCK_PREFIX % qualified_table_id

    def _memcache_get(self):
        return memcache.get(self._mem_key)

    def _memcache_add(self, value):
        return value, memcache.add(self._mem_key, value, time=self._MEM_LOCK_TIMEOUT)

    def _memcache_set(self, value):
        return value, memcache.set(self._mem_key, value, time=self._MEM_LOCK_TIMEOUT)

    def _memcache_delete(self, value):
        return value, memcache.delete(self._mem_key, seconds=self._MEM_LOCK_COOLDOWN) == 2

    def acquire(self):
        u"""Attempts to acquire a lock on the instance ``_qualified_table_id``.

        Returns:
            An unique lock value on success, or ``False`` if the method fails to acquire a lock.
        """

        value = self._memcache_get()
        if value:
            logging.error('[BqMultiprocessingLock.acquire] Lock \'%s\' set for table ID \'%s\'.'
                          % (value, self._qualified_table_id))
            return False
        else:
            lock, success = self._memcache_add(str(formatting.timestamp_to_posix(datetime.datetime.today())))
            if not success:
                logging.error(
                    '[BqMultiprocessingLock.acquire] Failed to acquire lock \'%s\' on table \'%s\'. Cooldown active?'
                    % (lock, self._qualified_table_id))
                return False
            else:
                logging.debug('[BqMultiprocessingLock.acquire] Acquired lock \'%s\' for table ID \'%s\'.'
                              % (lock, self._qualified_table_id))
                return lock

    def verify(self, lock=None):
        u"""Verifies (and refreshes) the Memcache lock.

        Args:
            lock: Lock value – we'll fall back on the instance ``_lock_value`` variable if unset.

        Returns:
            The lock value.

        Raises:
            AssertionError when the ``lock`` argument and the ``_lock_value`` instance variable are both falsy.
            LockCompromisedError if the lock value has been overwritten or evicted from Memcache due to timeout or
                utilization pressure.
            LockUpdateNetworkError on Memcache update/refresh error.
        """

        lock = lock or self._lock_value
        assert lock is not None

        mem_lock = self._memcache_get()
        if mem_lock != lock:
            raise LockCompromisedError(
                'Verification failed for lock \'%s\' (memcache lock set to \'%s\').' % (lock, mem_lock))
        else:
            lock, success = self._memcache_set(lock)
            if success:
                logging.debug('[BqMultiprocessingLock.verify] Verified and refreshed lock \'%s\' for table ID \'%s\'.'
                              % (lock, self._qualified_table_id))
            else:
                raise LockUpdateNetworkError('Refresh failed for multi-processing lock \'%s\'.' % lock)

        return lock

    def release(self, lock=None):
        u"""Foo"""

        lock = lock or self._lock_value
        assert lock is not None

        mem_lock = self._memcache_get()
        if mem_lock != lock:
            raise ValueError('Release cancelled for lock \'%s\' (new memcache lock set to \'%s\').' % (lock, mem_lock))
        else:
            lock, success = self._memcache_delete(lock)
            if success:
                logging.debug('[BqMultiprocessingLock.release] Successfully released lock \'%s\' on table ID \'%s\'.'
                              % (lock, self._qualified_table_id))
            else:
                logging.error('[BqMultiprocessingLock.release] Release of lock \'%s\' failed for table ID \'%s\'.'
                              % (lock, self._qualified_table_id))

        return lock
