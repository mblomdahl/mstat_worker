# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.email_sync
# Author:  Mats Blomdahl
# Version: 2013-11-03

u"""mstat_worker.email_sync module

Usage:
    Invoke URLs at ``/backend/worker/email_sync/<cmd>`` using the GAE TaskQueue or Cron Service.
"""

from email_sync_handler import EmailSyncHandler
