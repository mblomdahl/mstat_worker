# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.email_sync.EmailSyncHandler
# Author:  Mats Blomdahl
# Version: 2013-11-30

import json
import copy
import base64
import pickle
import logging
import time
import pprint

from google.appengine.api import taskqueue
from google.appengine.api import memcache

from google.appengine.ext import ndb

import ecore.constants as ecore_constants

import mstat_model.transactions as transaction_model

import mstat_constants as constants

import mstat_generic_handler as generic_handler

import gmail_fetcher


_DEFAULT_PAGESIZE = 25

_DEFAULT_SOURCING_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_SOURCING_QUEUE)

_IMAP_SOURCING_QUEUE = taskqueue.Queue(constants.WORKER_EMAIL_SOURCING_QUEUE)
_IMAP_PROCESSING_QUEUE = taskqueue.Queue(constants.WORKER_EMAIL_PROCESSING_QUEUE)
_PARSER_EMAIL_PROCESSING_QUEUE = taskqueue.Queue(constants.PARSER_EMAIL_PROCESSING_QUEUE)
_CACHE_MEM_KEY_PREFIX = 'WORKER_EMAIL_SYNC_CACHE_'
_CACHE_MEM_TIMEOUT = 600
_EMAIL_ACCOUNTS_PATH = '%s/resources/private/json/gmail_accounts.json' % constants.APP_ROOT_PATH


def _get_email_accounts(email_address):
    u"""Reads email account configurations from protected storage.

    Returns:
        A 2-tuple like so:

            (email_address, {'gmail_account': account_name,
                             'gmail_password': account_password,
                             'folders': {'default': [folder_name], 'shame': [folder_name]}})

        Or (email_address, False) if ``email_address`` is undefined.
    """

    with file(_EMAIL_ACCOUNTS_PATH, 'rb') as f:
        address_cfgs = json.loads(f.read())['email_addresses']
        f.close()

    return (email_address, address_cfgs[email_address]) if email_address in address_cfgs else (email_address, False)


def _generate_cache_mem_key(import_path, imap_folder, imap_uid, slice_=None):
    u"""Generates key for the GAE Memcache Service."""

    mem_key_postfix = '_%s_%s_%s_%i' % (import_path, imap_folder, imap_uid, int(time.time()))

    if slice_ is None:
        return '%s%s' % (_CACHE_MEM_KEY_PREFIX, mem_key_postfix)
    else:
        return '%s%s_%i' % (_CACHE_MEM_KEY_PREFIX, mem_key_postfix, slice_)


class EmailSyncHandler(generic_handler.CommonTaskQueueInterface):
    u"""Handlers maintaining `system@maklarstatistik.se` mailbox in sync with GAE Datastore representation.

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_worker/handlers.yaml` config).
    """

    # Interface: /backend/worker/email_sync/init_imap_sync
    def init_imap_sync(self):
        u"""Initializes IMAP sync for `email_address`. Queues sync jobs for each IMAP folder defined for
            `folders_category`.

        Raises:
            AssertionError on invalid request parameters for `email_address`, `write_queue` or `folders_category`.
        """

        email_address = self.request.get('email_address', default_value=None)
        assert email_address is not None

        folders_category = self.request.get('folders_category', default_value='default')
        assert folders_category in ['default', 'shame']

        write_queue = self.request.get('write_queue', default_value=constants.PARSER_JSON_PROCESSING_QUEUE)
        assert write_queue in [constants.PARSER_JSON_PROCESSING_QUEUE, constants.PARSER_SLOW_PROCESSING_QUEUE]

        email_address, account_cfg = _get_email_accounts(email_address)
        assert isinstance(account_cfg, dict)

        for category, imap_folders in account_cfg['folders'].iteritems():
            if folders_category == category:
                for imap_folder in imap_folders:
                    # Tasks will check one folder each, synchronously.
                    logging.info('[EmailSyncProcess.init_imap_sync] Dispatching sync job for IMAP folder %r within '
                                 'account %r...' % (imap_folder, account_cfg['gmail_account']))
                    _IMAP_SOURCING_QUEUE.add(taskqueue.Task(
                        url='/backend/worker/email_sync/sync_imap_folder',
                        payload=json.dumps({
                            'import_path': email_address,
                            'gmail_account': account_cfg['gmail_account'],
                            'gmail_password': account_cfg['gmail_password'],
                            'imap_folder': imap_folder,
                            'write_queue': write_queue
                        })
                    ))

    # Interface: /backend/worker/email_sync/sync_imap_folder
    def sync_imap_folder(self):
        u"""Retrieves IMAP UIDs for `imap_folder` and organizes them into batches for submission to the
            ``dispatch_imap_reads`` hook.
        """

        task_data = json.loads(self.request.body)

        fetcher = gmail_fetcher.GmailFetcher(task_data['gmail_account'], task_data['gmail_password'])

        task_data['imap_uid_validity'] = fetcher.select_folder(task_data['imap_folder'])['UIDVALIDITY']

        message_uids = fetcher.get_message_uids()
        logging.info('[EmailSyncProcess.sync_imap_folder] Found a total of %i message UIDs within IMAP folder \'%s\' at'
                     ' \'%s\'.' % (len(message_uids), task_data['imap_folder'], task_data['gmail_account']))

        fetcher.logout()

        batch_size = _DEFAULT_PAGESIZE * 5
        batches = []
        while len(message_uids) > batch_size:
            batches.append(message_uids[:batch_size])
            message_uids = message_uids[batch_size:]
        else:
            if len(message_uids):
                batches.append(message_uids)

        for message_uids in batches:
            batch_data = copy.copy(task_data)
            batch_data['message_uids'] = message_uids

            # Each task will process up to ``batch_size`` message UIDs each to locate any new content.
            _IMAP_PROCESSING_QUEUE.add(taskqueue.Task(
                url='/backend/worker/email_sync/dispatch_imap_reads',
                payload=json.dumps(batch_data)
            ))

    # Interface: /backend/worker/email_sync/dispatch_imap_reads
    def dispatch_imap_reads(self):
        u"""Queries NDB for existing records matching `message_uids`. Non-existing UIDs are sorted out and dispatched to
            ``read_imap_messages`` for ingestion.
        """

        email_kind = transaction_model.CapitexRawDataEmail

        task_data = json.loads(self.request.body)
        message_uids = task_data.pop('message_uids')

        batch_size = _DEFAULT_PAGESIZE
        batches = []
        while len(message_uids) > batch_size:
            batches.append(message_uids[:batch_size])
            message_uids = message_uids[batch_size:]
        else:
            if len(message_uids):
                batches.append(message_uids)

        new_uids = []
        for batch_uids in batches:
            batch_futures = []
            for imap_uid in batch_uids:
                batch_futures.append(
                    (imap_uid, email_kind.query(ndb.AND(email_kind.imap_account == task_data['gmail_account'],
                                                        email_kind.imap_folder == task_data['imap_folder'],
                                                        email_kind.imap_uid_validity == task_data['imap_uid_validity'],
                                                        # TODO(mats.blomdahl@gmail.com): Shame! Use ctx.
                                                        email_kind.imap_uid == imap_uid)).get_async(deadline=3600)))

            ndb.Future.wait_all([tuple_[1] for tuple_ in batch_futures])

            for imap_uid, future in batch_futures:
                if future.get_result() is None:
                    # TODO(mats.blomdahl@gmail.com): Add check for presence in the malformed emails pull queue.
                    new_uids.append(imap_uid)

        if any(new_uids):
            logging.info('[EmailSyncProcess.dispatch_imap_reads] Identified %i new UIDs among the %i UIDs evaluated '
                         'from IMAP folder \'%s\' at \'%s\' (new_uids: \'%s\').' % (len(new_uids), len(message_uids),
                                                                                    task_data['imap_folder'],
                                                                                    task_data['gmail_account'],
                                                                                    pprint.pformat(new_uids)))
            task_data['new_uids'] = new_uids
            _IMAP_SOURCING_QUEUE.add(taskqueue.Task(
                url='/backend/worker/email_sync/read_imap_messages',
                payload=json.dumps(task_data)
            ))

    # Interface: /backend/worker/email_sync/read_imap_messages
    def read_imap_messages(self):
        u"""Performs initial ingestion of IMAP messages referenced by the task payload.

        Each message is dispatched to the `mstat_parser` module's email parser. Small message objects solely reside in
            the output task payload while large emails are sliced into one or more Memcache items during transport.
        """

        task_data = json.loads(self.request.body)

        import_path = task_data['import_path']
        gmail_account = task_data['gmail_account']
        imap_folder = task_data['imap_folder']
        imap_uid_validity = task_data['imap_uid_validity']
        write_queue = task_data['write_queue']

        fetcher = gmail_fetcher.GmailFetcher(gmail_account, task_data['gmail_password'])
        fetcher.select_folder(imap_folder, uid_validity=imap_uid_validity)

        new_messages = fetcher.fetch_messages(task_data['new_uids'])

        fetcher.logout()

        mem_client = memcache.Client()

        # TODO(mats.blomdahl@gmail.com): Identify suitable insertion point for content-based hash eval.
        for imap_uid, message in new_messages.iteritems():
            logging.debug(
                '[EmailSyncProcess.read_imap_messages] Dispatching parsing op. for new message UID %i...' % imap_uid)

            base_payload = {
                'import_path': import_path,
                'imap_account': gmail_account,
                'imap_folder': imap_folder,
                'imap_uid': imap_uid,
                'imap_uid_validity': imap_uid_validity,
                'write_queue': write_queue
            }

            mem_payload, mem_keys, mem_slice = copy.copy(base_payload), [], 0
            mem_payload['message'] = message
            mem_payload = base64.urlsafe_b64encode(pickle.dumps(mem_payload))

            if len(mem_payload) > ecore_constants.TASK_QUEUE_MAX_PAYLOAD_SIZE:
                # Use Memcache transport for large payloads.
                base_payload['payload_length'] = len(mem_payload)

                logging.debug(
                    '[EmailSyncProcess.read_imap_messages] TaskQueue payload limits exceeded for message UID %i -> '
                    'Resorting to Memcache transport (payload_length: %i).' % (imap_uid, len(mem_payload)))

                def _put_payload(mem_key, payload_slice):
                    mem_client.set(mem_key, payload_slice, time=_CACHE_MEM_TIMEOUT*10)

                payload_length = 0
                while len(mem_payload) > ecore_constants.MEMCACHE_MAX_ENTRY_SIZE:
                    output_slice = mem_payload[:ecore_constants.MEMCACHE_MAX_ENTRY_SIZE]
                    mem_payload = mem_payload[ecore_constants.MEMCACHE_MAX_ENTRY_SIZE:]
                    mem_keys.append(_generate_cache_mem_key(import_path, imap_folder, imap_uid, slice_=mem_slice))
                    _put_payload(mem_keys[-1], output_slice)
                    payload_length += len(output_slice)
                    mem_slice += 1

                else:
                    mem_keys.append(_generate_cache_mem_key(import_path, imap_folder, imap_uid, slice_=mem_slice))
                    _put_payload(mem_keys[-1], mem_payload)
                    payload_length += len(mem_payload)

                logging.debug(
                    '[EmailSyncProcess.read_imap_messages] Payload written to Memcache; a total %i bytes across %i '
                    'item(s) (mem_keys: \'%s\').' % (payload_length, mem_slice, pprint.pformat(mem_keys)))
                assert payload_length == base_payload['payload_length']

                task_payload = copy.copy(base_payload)
                task_payload['mem_keys'] = mem_keys

            else:
                task_payload = copy.copy(base_payload)
                task_payload['message'] = message

            _PARSER_EMAIL_PROCESSING_QUEUE.add(taskqueue.Task(
                url='/backend/emailparser/parse_capitex_email',
                payload=json.dumps(task_payload)
            ))

    # # Interface: maklarstatistik.appspot.com/backend/worker/email_sync/dumpsdecjanfeb?sys_broken=True&sys_discarded=True
    # def dumpsdecjanfeb(self):
    #     """Foo"""
    #
    #     import datetime
    #
    #     kind_cls = transaction_model.CapitexRawDataEmail
    #     kind_cls_read_policy = kind_cls.get_default_export_consistency()
    #
    #     sys_broken = eval(self.request.get('sys_broken', default_value='True'))
    #     sys_discarded = eval(self.request.get('sys_discarded', default_value='True'))
    #
    #     query = kind_cls.query(ndb.AND(kind_cls.sys_discarded == sys_discarded,
    #                                    kind_cls.sys_broken == sys_broken,
    #                                    kind_cls.date > datetime.datetime(2013, 11, 27)))
    #
    #     has_more, cursor = True, None
    #     all_entries = []
    #     while has_more:
    #         entries, cursor, has_more = query.fetch_page(250,
    #                                                      batch_size=250,
    #                                                      start_cursor=cursor,
    #                                                      read_policy=kind_cls_read_policy)
    #         all_entries.extend(entries)
    #     self.response.write('<html><body>')
    #     for e in all_entries:
    #         self.response.write('%s;%s;%s;%s<br>' % (e.date.isoformat(), e.sender, e.dataset_origin_sender, e.subject))
    #         if e.attachment_filenames:
    #             [self.response.write('%s<br>' % name) for name in e.attachment_filenames]
    #     self.response.write('</body></html>')
