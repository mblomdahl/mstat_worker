# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.email_sync.GmailFetcher
# Author:  Mats Blomdahl
# Version: 2014-02-04

import logging
import email
import imaplib

import ecore.imapclient as imapclient

_HOST = 'imap.gmail.com'
_PORT = 993
_SSL = True


class ImapMessageParsingError(BaseException):
    u"""Raised to signal IMAP email message parsing error."""


class GmailFetcher(object):
    u"""Utility class providing a wrapper for the `imapclient` library by Menno Smits (2013).

    This is _not_ a general-purpose wrapper – it's strictly geared toward fetching UUIDs and email messages from Gmail
        in a read-only context.

    Usage:
        >>> fetcher = GmailFetcher('me@gmail.com', 'my-super-secret-password')
        >>> fetcher.select_folder('INBOX')
        >>> message_uids = fetcher.get_message_uids()
        >>> fetcher.logout()
    """

    def __init__(self, username, password, host=None, port=None, ssl=None, readonly=True, auto_login=True):
        u"""Initializes instance attributes and automatically connects to Gmail."""

        self._username, self._password = username, password

        self._host, self._port = host or _HOST, port or _PORT

        self._ssl = ssl if ssl is not None else _SSL

        self._readonly = readonly
        self._server = None
        self._folder = None

        if auto_login:
            self.login()

    def _connect(self):
        u"""Establishes connection to remote server."""

        logging.debug('[GmailFetcher._connect] Connecting to host %r...' % self._host)

        self._server = imapclient.IMAPClient(self._host, port=self._port, use_uid=True, ssl=self._ssl)

        logging.info('[GmailFetcher._connect] Connection established.')

    def login(self):
        u"""Performs login using the credentials stored among instance attributes.

        Returns:
            The server `login response`, a string.
        """

        self._connect()

        logging.debug('[GmailFetcher.login] Login initiated...')

        login_output = self._server.login(self._username, self._password)

        logging.info('[GmailFetcher.login] Login completed (login_output: \'%r\').' % login_output)

        return login_output

    def logout(self):
        u"""Performs a logout.

        Returns:
            The server `logout response' or ``None`` if the logout op. raises an ``imaplib.IMAP4.abort`` exception.
        """

        logging.debug('[GmailFetcher.logout] Logout initiated...')

        if self._folder:
            try:
                self.close_folder()
            except imaplib.IMAP4.abort as e:
                logging.error('[GmailFetcher.logout] self.close_folder() raised IMAP4.abort error: %r' % e.message)

        try:
            logout_output = self._server.logout()
            logging.debug('[GmailFetcher.logout] Logout completed (IMAP output: \'%r\').' % logout_output)
            self._server = None
            return logout_output

        except imaplib.IMAP4.abort as e:
            logging.error('[GmailFetcher.logout] self._server.logout() raised IMAP4.abort error: %r' % e.message)
            self._server = None
            return None

    def select_folder(self, folder, uid_validity=None):
        u"""Selects target IMAP folder.

        Args:
            folder: The folder name.
            uid_validity: Optional UID validity parameter to verify UID state.

        Returns:
            The server `select folder response`, a string.

        Raises:
            AssertionError on UID validation failure.
        """

        logging.debug('[GmailFetcher.select_folder] Opening folder \'%s\'...' % folder)

        # Close any previously configured folder.
        if self._folder:
            self.close_folder()

        select_output = self._server.select_folder(folder, readonly=self._readonly)
        assert uid_validity is None or select_output['UIDVALIDITY'] == uid_validity
        self._folder = folder

        logging.debug(
            '[GmailFetcher.select_folder] Folder \'%s\' opened (select_output: \'%s\').' % (folder, select_output))

        return select_output

    def close_folder(self):
        u"""Closes the selected IMAP folder.

        Returns:
            The server `close folder response`, a string.

        Raises:
            AssertionError when there isn't any folder to close.
        """

        assert self._folder is not None

        folder = self._folder
        logging.debug('[GmailFetcher.close_folder] Closing folder \'%s\'...' % folder)
        close_output = self._server.close_folder()
        self._folder = None

        logging.debug(
            '[GmailFetcher.close_folder] Folder \'%s\' closed (close_output: \'%s\').' % (folder, close_output))

        return close_output

    def get_message_uids(self):
        u"""Searches the selected IMAP folder for message UIDs.

        Returns:
            A list of message UIDs.

        Raises:
            AssertionError on invalid server or folder config.
        """

        assert isinstance(self._server, imapclient.IMAPClient) and self._folder

        logging.debug('[GmailFetcher.get_message_uids] Querying for message UIDs in IMAP folder \'%s\'.' % self._folder)

        imap_uids = self._server.search(criteria=['NOT DELETED'])

        logging.debug('[GmailFetcher.get_message_uids] Retrieved a total of %i messages from IMAP folder \'%s\'.'
                      % (len(imap_uids), self._folder))

        return imap_uids

    def fetch_messages(self, message_uids, output_imap_uid=True):
        u"""Retrieves messages from UIDs in the selected IMAP folder.

        Args:
            message_uids: A list of message UIDs.
            output_imap_uid: Outputs message identifier as UID instead of `Message-ID` email header.

        Returns:
            A dict mapping of message identifiers to message details.

        Raises:
            AssertionError on invalid server or folder config.
            ImapMessageParsingError on message parsing error.
        """

        assert isinstance(self._server, imapclient.IMAPClient) and self._folder

        logging.debug('[GmailFetcher.fetch_messages] Fetching %i messages in IMAP folder \'%s\'.' % (len(message_uids),
                                                                                                     self._folder))

        messages, output = self._server.fetch(message_uids, ['RFC822', 'INTERNALDATE', 'RFC822.HEADER', 'FLAGS']), {}
        for imap_uid, message in messages.iteritems():
            try:
                output_value = {
                    'RFC822': message['RFC822'],
                    'RFC822.HEADER': message['RFC822.HEADER'],
                    'INTERNALDATE': message['INTERNALDATE'].isoformat(),
                    'FLAGS': message['FLAGS']
                }
                if output_imap_uid:
                    output[imap_uid] = output_value
                else:
                    message_id = email.message_from_string(message['RFC822.HEADER'])['Message-ID']
                    output[message_id] = output_value
            except:
                raise ImapMessageParsingError('Parsing email (imap_uid=\'%i\') failed.' % imap_uid)

        logging.debug('[GmailFetcher.fetch_messages] Retrieved a total of %i messages from IMAP folder \'%s\'.'
                      % (len(message_uids), self._folder))

        return output


