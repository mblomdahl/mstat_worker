# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.memcache_sync.MemcacheSyncHandler
# Author:  Mats Blomdahl
# Version: 2014-01-22

import datetime
import json
import logging
import random

from google.appengine.api import taskqueue
from google.appengine.ext import ndb
from google.appengine.datastore.datastore_query import Cursor

import ecore.formatting as formatting

import mstat_model.memcache as memcache_model
import mstat_model.user as user_model

import mstat_constants as constants

import mstat_generic_handler as generic_handler


_DEFAULT_PAGESIZE = 25

_DEFAULT_SOURCING_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_SOURCING_QUEUE)
_DEFAULT_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_PROCESSING_QUEUE)
_NDB_MEMCACHE_REFRESH_QUEUE = taskqueue.Queue(constants.WORKER_NDB_MEMCACHE_REFRESH_QUEUE)


class MemcacheSyncHandler(generic_handler.CommonTaskQueueInterface):
    u"""Handlers maintaining GAE Memcache resources in sync.

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_worker/handlers.yaml` config).
    """

    # Interface: /backend/worker/memcache_sync/init_tce_aggregate_population
    def init_tce_aggregate_population(self):
        u"""Initializes population of the ``TceAggregate`` kind for contract days in range [2005-01-01, 2015-12-31]."""

        for year in range(2005, 2015):
            start_date = datetime.date(year, 1, 1)
            end_date = datetime.date(year + 1, 1, 1)
            _DEFAULT_QUEUE.add(taskqueue.Task(
                url='/backend/worker/memcache_sync/populate_tce_aggregates_by_date_range',
                params={
                    'start_date': start_date.isoformat(),
                    'end_date': end_date.isoformat()
                }
            ))

    # Interface: /backend/worker/memcache_sync/populate_tce_aggregates_by_date_range
    def populate_tce_aggregates_by_date_range(self):
        u"""Queues population of ``TceAggregate`` entries for all days in range [`start_date`, `end_date`)."""

        start_date = formatting.coerce_to_date(self.request.get('start_date', default_value=None))
        end_date = formatting.coerce_to_date(self.request.get('end_date', default_value=None))

        while start_date < end_date:
            _DEFAULT_QUEUE.add(taskqueue.Task(
                url='/backend/worker/memcache_sync/process_tce_aggregate_create_targets',
                payload=json.dumps({'target_dates': [start_date.isoformat()]})
            ))

            start_date += datetime.timedelta(days=1)

    # Interface: /backend/worker/memcache_sync/process_tce_aggregate_refresh_targets
    def process_tce_aggregate_create_targets(self):
        u"""Performs population of ``TceAggregate`` entries for all contract days in `target_dates`."""

        for date_str in json.loads(self.request.body)['target_dates']:
            memcache_model.TceAggregate.create(formatting.coerce_to_date(date_str))

    # Interface: /backend/worker/memcache_sync/source_tce_aggregate_refresh_targets
    def source_tce_aggregate_refresh_targets(self):
        u"""Locates dirty ``TceAggregate`` entries for cache updates and maintains the kind entries up-to-date within
            the NDB memcache.

        Raises:
            AssertionError on invalid `refresh` request parameter.
        """

        _TCE_T0 = datetime.date(2005, 1, 1)  # t(0)

        cursor = self.request.get('cursor', default_value=None)
        start_date = self.request.get('start_date', default_value=None)
        start_date = formatting.coerce_to_date(start_date) if start_date else _TCE_T0

        end_date = self.request.get('end_date', default_value=None)
        end_date = formatting.coerce_to_date(end_date) if end_date else datetime.date.today()
        refresh = self.request.get('refresh', default_value='dirty')

        maintenance_t0 = self.request.get('maintenance_t0', default_value=None)
        maintenance_t0 = formatting.coerce_to_date(maintenance_t0) if maintenance_t0 else _TCE_T0

        maintenance_batch_size = int(self.request.get('maintenance_batch_size', default_value='0'))

        aggregate_kind, has_more, cursor = memcache_model.TceAggregate, True, Cursor(urlsafe=cursor)

        if refresh == 'all':
            logging.debug('[MemcacheSyncProcess.source_tce_aggregate_refresh_targets] Querying for _all_ entries in '
                          'range %s--%s...' % (start_date.isoformat(), end_date.isoformat()))
            query = aggregate_kind.query(ndb.AND(aggregate_kind.contract_date >= start_date,
                                                 aggregate_kind.contract_date < end_date))
        elif refresh == 'dirty':
            logging.debug('[MemcacheSyncProcess.source_tce_aggregate_refresh_targets] Querying for dirty entries in '
                          'range %s--%s...' % (start_date.isoformat(), end_date.isoformat()))
            query = aggregate_kind.query(ndb.AND(aggregate_kind.contract_date >= start_date,
                                                 aggregate_kind.contract_date < end_date,
                                                 aggregate_kind.sys_dirty == True))
        else:
            raise AssertionError('Invalid input for \'refresh\' param %r' % refresh)

        ndb_keys, cursor, has_more = query.fetch_page(_DEFAULT_PAGESIZE, batch_size=_DEFAULT_PAGESIZE, deadline=3600,
                                                      keys_only=True, start_cursor=cursor)

        for key in ndb_keys:
            logging.debug('[MemcacheSyncProcess.source_tce_aggregate_refresh_targets] Queueing re-caching of aggregate '
                          'key ID \'%s\'.' % key.id())
            _NDB_MEMCACHE_REFRESH_QUEUE.add(taskqueue.Task(
                url='/backend/worker/memcache_sync/process_tce_aggregate_refresh_targets',
                payload=json.dumps({'target_dates': [aggregate_kind.target_date_from_key(key).isoformat()]})
            ))

        if has_more:
            _DEFAULT_SOURCING_QUEUE.add(taskqueue.Task(
                url='/backend/worker/memcache_sync/source_tce_aggregate_refresh_targets',
                params={
                    'cursor': cursor.urlsafe(),
                    'start_date': start_date.isoformat(),
                    'end_date': end_date.isoformat(),
                    'refresh': refresh
                }
            ))
        elif maintenance_batch_size:
            # Cache random months
            t0_months_d = (end_date.year - maintenance_t0.year) * 12 + end_date.month  # i.e. 13-12-14 ≈ 05-01-01 + 108m
            if maintenance_batch_size > t0_months_d:
                recache_rand_months = random.sample(range(t0_months_d), maintenance_batch_size)
            else:
                recache_rand_months = range(t0_months_d)

            for month_delta in recache_rand_months:
                upper_lim = formatting.dt_monthdelta(end_date, -month_delta).isoformat()
                lower_lim = formatting.dt_monthdelta(end_date, -(month_delta + 1)).isoformat()

                logging.debug('[MemcacheSyncProcess.source_tce_aggregate_refresh_targets] Queueing re-caching of random'
                              'month (%s--%s)...' % (lower_lim, upper_lim))
                _DEFAULT_SOURCING_QUEUE.add(taskqueue.Task(
                    url='/backend/worker/memcache_sync/source_tce_aggregate_cache_update',
                    params={'start_date': lower_lim, 'end_date': upper_lim}
                ))

    # Interface: /backend/worker/memcache_sync/process_tce_aggregate_refresh_targets
    def process_tce_aggregate_refresh_targets(self):
        u"""Performs ``TceAggregate`` refresh."""

        target_date_strs = json.loads(self.request.body)['target_dates']

        for date_str in target_date_strs:
            memcache_model.TceAggregate.refresh(formatting.coerce_to_date(date_str))

    # Interface: /backend/worker/memcache_sync/source_tce_aggregate_cache_update
    def source_tce_aggregate_cache_update(self):
        u"""Queues ``TceAggregate`` entries in range [`start_date`, `end_date`) for NDB memcache maintenance."""

        cursor = self.request.get('cursor', default_value=None)
        start_date = self.request.get('start_date', default_value=None)

        end_date = self.request.get('end_date', default_value=None)
        end_date = datetime.date.today() if end_date is None else formatting.coerce_to_date(end_date)

        processing_queue = self.request.get('processing_queue', default_value=None)
        if processing_queue:
            assert processing_queue in (constants.WORKER_NDB_MEMCACHE_REFRESH_QUEUE,
                                        constants.WORKER_SYNC_PROCESSING_QUEUE)
            processing_queue = taskqueue.Queue(processing_queue)
        else:
            processing_queue = _NDB_MEMCACHE_REFRESH_QUEUE

        aggregate_kind, has_more, cursor = memcache_model.TceAggregate, True, Cursor(urlsafe=cursor)

        if start_date:
            start_date = formatting.coerce_to_date(start_date)
            query = aggregate_kind.query(ndb.AND(aggregate_kind.contract_date >= start_date,
                                                 aggregate_kind.contract_date < end_date))
        else:
            query = aggregate_kind.query(aggregate_kind.contract_date < end_date)
        ndb_keys, cursor, has_more = query.fetch_page(_DEFAULT_PAGESIZE, batch_size=_DEFAULT_PAGESIZE, deadline=3600,
                                                      keys_only=True, start_cursor=cursor)

        for key in ndb_keys:
            processing_queue.add(taskqueue.Task(
                url='/backend/worker/memcache_sync/process_tce_aggregate_cache_update',
                payload=json.dumps({'ndb_keys_urlsafe': [key.urlsafe()]})
            ))

        if has_more:
            _DEFAULT_SOURCING_QUEUE.add(taskqueue.Task(
                url='/backend/worker/memcache_sync/source_tce_aggregate_cache_update',
                params={
                    'cursor': cursor.urlsafe(),
                    'end_date': end_date.isoformat()
                }
            ))

    # Interface: /backend/worker/memcache_sync/process_tce_aggregate_cache_update
    def process_tce_aggregate_cache_update(self):
        u"""Performs NDB memcache maintenance for entities of the ``TceAggregate`` kind referenced by
            `ndb_keys_urlsafe`."""

        ndb_keys_urlsafe = json.loads(self.request.body)['ndb_keys_urlsafe']

        memcache_model.TceAggregate.to_ndb_memcache([ndb.Key(urlsafe=key) for key in ndb_keys_urlsafe],
                                                    iops_throttle=True)

    # Interface: /backend/worker/memcache_sync/delete_tce_aggregate_shard
    def delete_tce_aggregate_shard(self):
        u"""Performs NDB sync deletes for ``TceAggregateShard`` entities referenced by `ndb_keys_urlsafe`."""

        ndb_keys_urlsafe = json.loads(self.request.body)['ndb_keys_urlsafe']

        # TODO(mats.blomdahl@gmail.com): Shame! Use ctx.
        [ndb.Key(urlsafe=key).delete(deadline=3600) for key in ndb_keys_urlsafe]

    # Interface: /backend/worker/memcache_sync/source_user_model_cache_update
    def source_user_model_cache_update(self):
        u"""Locates and queues entities of the ``OrganizationAccount`` sub-kinds for NDB memcache maintenance."""

        def _queue(key):
            _NDB_MEMCACHE_REFRESH_QUEUE.add(taskqueue.Task(
                url='/backend/worker/memcache_sync/process_user_model_cache_update',
                params={'ndb_key_urlsafe': key.urlsafe()}
            ))

        subscriber_kind, sysadmin_kind = user_model.SubscriberAccount, user_model.SystemAdminAccount

        cursor, has_more = None, True
        while has_more:
            results, cursor, has_more = subscriber_kind.query().fetch_page(_DEFAULT_PAGESIZE,
                                                                           batch_size=_DEFAULT_PAGESIZE,
                                                                           keys_only=True,
                                                                           start_cursor=cursor)
            [_queue(key) for key in results]

        cursor, has_more = None, True
        while has_more:
            results, cursor, has_more = sysadmin_kind.query().fetch_page(_DEFAULT_PAGESIZE,
                                                                         batch_size=_DEFAULT_PAGESIZE,
                                                                         keys_only=True,
                                                                         start_cursor=cursor)
            [_queue(key) for key in results]

    # Interface: /backend/worker/memcache_sync/process_user_model_cache_update
    def process_user_model_cache_update(self):
        u"""Performs NDB memcache maintenance for a ``OrganizationAccount`` entity referenced by `ndb_key_urlsafe`."""

        organization_ndb_key = ndb.Key(urlsafe=self.request.get('ndb_key_urlsafe'))

        organization_ndb_key.get().to_ndb_memcache(iops_throttle=True)

