# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.memcache_sync
# Author:  Mats Blomdahl
# Version: 2013-11-03

u"""mstat_worker.memcache_sync module

Usage:
    Invoke URLs at ``/backend/worker/memcache_sync/<cmd>`` using the GAE TaskQueue or Cron Service.
"""

from memcache_sync_handler import MemcacheSyncHandler
