# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.memcache_sync.StatsSyncHandler
# Author:  Mats Blomdahl
# Version: 2014-02-01

import datetime

from google.appengine.api import taskqueue
from google.appengine.ext import ndb
from google.appengine.datastore.datastore_query import Cursor

import ecore.formatting as formatting

import mstat_model.transactions as transaction_model

import mstat_constants as constants

import mstat_generic_handler as generic_handler


_DEFAULT_PAGESIZE = 25

_DEFAULT_SOURCING_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_SOURCING_QUEUE)
_DEFAULT_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_PROCESSING_QUEUE)

_TRACKED_KINDS = [
    transaction_model.SfRawDataStats,
    transaction_model.SfdRawDataStats,
    transaction_model.ScbRawDataStats,
    transaction_model.CapitexRawDataStats,
    transaction_model.LfRawDataStats,
    transaction_model.TransactionCompositeStats
]


class StatsSyncHandler(generic_handler.CommonTaskQueueInterface):
    u"""Handlers keeping transaction metadata and statistical calculations up-to-date.

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_worker/handlers.yaml` config).
    """

    # Interface: /backend/worker/stats_sync/init_stats_population
    def init_stats_population(self):
        u"""Queues stats initialization jobs for all dates in range [`start_year`, `end_year`)."""

        start_year = self.request.get('start_year', default_value=None)
        end_year = self.request.get('end_year', default_value=None)
        kind = self.request.get('kind', default_value=None)

        base_params = {'kind': kind} if kind else {}

        if start_year is None and end_year is None:
            for year in range(1990, 2020):
                base_params.update({'start_year': '%i' % year, 'end_year': '%i' % (year + 1)})
                _DEFAULT_QUEUE.add(taskqueue.Task(
                    url='/backend/worker/stats_sync/init_stats_population',
                    params=base_params
                ))

        else:
            start_date, end_date = datetime.date(int(start_year), 1, 1), datetime.date(int(end_year), 1, 1)
            while start_date < end_date:
                base_params['target_date'] = start_date.isoformat()
                _DEFAULT_QUEUE.add(taskqueue.Task(
                    url='/backend/worker/stats_sync/populate_date',
                    params=base_params
                ))
                start_date += datetime.timedelta(days=1)

    # Interface: /backend/worker/stats_sync/populate_date
    def populate_date(self):
        u"""Initializes stats calculations for a given date and entity kind."""

        target_date = formatting.coerce_to_date(self.request.get('target_date', default_value=None))
        stats_kind = self.request.get('kind', default_value=None)
        if stats_kind:
            stats_kind_cls = eval('transaction_model.%s' % stats_kind)
            stats_kind_cls.create(target_date)

        else:
            for stats_kind_cls in _TRACKED_KINDS:
                stats_kind_cls.create(target_date)

    # Interface: /backend/worker/stats_sync/source_stats_sync_refresh_targets
    def source_stats_sync_refresh_targets(self):
        u"""Locates target dates tagged as `dirty` for all entity kinds in ``_TRACKED_KINDS`` and dispatches stats'
            refresh jobs to the GAE TaskQueue.

        Raises:
            AssertionError on invalid `refresh` request parameter.
        """

        _MSTAT_T0 = datetime.date(2005, 1, 1)

        start_date = self.request.get('start_date', default_value=None)
        start_date = formatting.coerce_to_date(start_date) if start_date else _MSTAT_T0

        end_date = self.request.get('end_date', default_value=None)
        end_date = formatting.coerce_to_date(end_date) if end_date else datetime.date.today()

        cursor = self.request.get('cursor', default_value=None)
        refresh = self.request.get('refresh', default_value='dirty')

        stats_kind = self.request.get('stats_kind', default_value=None)
        if stats_kind is None:
            for stats_kind_cls in _TRACKED_KINDS:
                _DEFAULT_SOURCING_QUEUE.add(taskqueue.Task(
                    url='/backend/worker/stats_sync/source_stats_sync_refresh_targets',
                    params={
                        'stats_kind': stats_kind_cls.get_kind_name(),
                        'start_date': start_date.isoformat(),
                        'end_date': end_date.isoformat(),
                        'refresh': refresh
                    }
                ))
            return

        stats_kind_cls, has_more, cursor = eval('transaction_model.%s' % stats_kind), True, Cursor(urlsafe=cursor)

        if refresh == 'all':
            query = stats_kind_cls.query(ndb.AND(stats_kind_cls.stats_calculation_date >= start_date,
                                                 stats_kind_cls.stats_calculation_date < end_date))
        elif refresh == 'dirty':
            query = stats_kind_cls.query(ndb.AND(stats_kind_cls.stats_calculation_date >= start_date,
                                                 stats_kind_cls.stats_calculation_date < end_date,
                                                 stats_kind_cls.sys_dirty == True))
        else:
            raise AssertionError('Invalid input for \'refresh\' param %r' % refresh)

        ndb_keys, cursor, has_more = query.fetch_page(_DEFAULT_PAGESIZE, batch_size=_DEFAULT_PAGESIZE, deadline=3600,
                                                      keys_only=True, start_cursor=cursor)

        for key in ndb_keys:
            _DEFAULT_QUEUE.add(taskqueue.Task(
                url='/backend/worker/stats_sync/process_stats_sync_refresh_targets',
                params={
                    'target_date': stats_kind_cls.target_date_from_key(key).isoformat(),
                    'stats_kind': stats_kind
                }
            ))

        if has_more:
            _DEFAULT_SOURCING_QUEUE.add(taskqueue.Task(
                url='/backend/worker/stats_sync/source_stats_sync_refresh_targets',
                params={
                    'cursor': cursor.urlsafe(),
                    'stats_kind': stats_kind,
                    'start_date': start_date.isoformat(),
                    'end_date': end_date.isoformat(),
                    'refresh': refresh
                }
            ))

    # Interface: /backend/worker/stats_sync/process_stats_refresh_targets
    def process_stats_sync_refresh_targets(self):
        u"""Performs a stats calculation's `create-or-refresh` for a given date and entity kind."""

        target_date = formatting.coerce_to_date(self.request.get('target_date', default_value=None))

        stats_kind = self.request.get('stats_kind', default_value=None)
        stats_kind_cls = eval('transaction_model.%s' % stats_kind)

        stats_kind_cls.refresh_or_create(target_date)
