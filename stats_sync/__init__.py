# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.stats_sync
# Author:  Mats Blomdahl
# Version: 2013-11-03

u"""mstat_worker.stats_sync module

Usage:
    Invoke URLs at ``/backend/worker/stats_sync/<cmd>`` using the GAE TaskQueue or Cron Service.
"""

from stats_sync_handler import StatsSyncHandler
