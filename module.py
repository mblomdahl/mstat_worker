# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.module.app
# Author:  Mats Blomdahl
# Version: 2014-02-18

from __future__ import absolute_import

import logging
import webapp2

import ecore

CONFIG = {}


def _handle_400(request, response, exception):
    logging.exception(exception)
    response.content_type = 'text/plain'
    response.write(
        'The server could not comply with the request since it is either malformed or otherwise incorrect.')
    response.set_status(400)


def _handle_404(request, response, exception):
    logging.exception(exception)
    response.content_type = 'text/plain'
    response.write('Oops! I could swear this page was here!')
    response.set_status(404)


def _handle_500(request, response, exception):
    logging.exception(exception)
    response.content_type = 'text/plain'
    response.write('A server error occurred!')
    response.set_status(500)


app = webapp2.WSGIApplication(routes=[
    # Handler starting the backend service.
    webapp2.Route('/_ah/start',
                  handler='ecore.handler.BackendStartup'),

    # Handlers performing IMAP sync ops.
    webapp2.Route('/backend/worker/email_sync/<process:\w+>',
                  handler='mstat_worker.email_sync.EmailSyncHandler:handle',
                  methods=['GET', 'POST']),

    # Handlers dispatching supplier web API consumption tasks.
    webapp2.Route('/backend/worker/supplier_api_sync/<process:\w+>',
                  handler='mstat_worker.supplier_api_sync.SupplierApiSyncHandler:handle',
                  methods=['GET', 'POST']),

    # Handlers performing ingestion of RV reassessment factors.
    webapp2.Route('/backend/worker/rv_reassessment/<process:\w+>',
                  handler='mstat_worker.rv_reassessment.RvReassessmentHandler:handle',
                  methods=['GET', 'POST']),

    # Handlers performing ingestion of the Svensk Postnummerservice address registry.
    webapp2.Route('/backend/worker/address_registry/<process:\w+>',
                  handler='mstat_worker.address_registry.AddressRegistryHandler:handle',
                  methods=['GET', 'POST']),

    # Handlers maintaining memcache in sync.
    webapp2.Route('/backend/worker/memcache_sync/<process:\w+>',
                  handler='mstat_worker.memcache_sync.MemcacheSyncHandler:handle',
                  methods=['GET', 'POST']),

    # Handlers keeping transaction metadata and statistics up-to-date.
    webapp2.Route('/backend/worker/stats_sync/<process:\w+>',
                  handler='mstat_worker.stats_sync.StatsSyncHandler:handle',
                  methods=['GET', 'POST']),

    # Handlers performing BigQuery sync tasks.
    webapp2.Route('/backend/worker/bq_sync/<process:\w+>',
                  handler='mstat_worker.bq_sync.BqSyncHandler:handle',
                  methods=['GET', 'POST']),

    # Handlers performing Cloud SQL sync tasks.
    webapp2.Route('/backend/worker/sql_sync/<process:\w+>',
                  handler='mstat_worker.sql_sync.SqlSyncHandler:handle',
                  methods=['GET', 'POST']),

    # Handlers performing Cloud SQL sync tasks.
    webapp2.Route('/backend/worker/tce_userspace_stats/<process:\w+>',
                  handler='mstat_worker.sql_sync.SqlSyncHandler:handle',
                  methods=['GET', 'POST']),

    # Handlers performing manual dataset updates.
    webapp2.Route('/backend/worker/<process:\w+>',
                  handler='mstat_worker.RootHandler:handle',
                  methods=['GET', 'POST']),

    # Demo hook for Peter at FasAd
    webapp2.Route('/supplier_api/fasad/<process:\w+>',
                  handler='mstat_worker.supplier_api.SupplierApiHandler:handle',
                  methods=['GET', 'POST'])

], config=CONFIG, debug=True)

app.error_handlers[400] = _handle_400
app.error_handlers[404] = _handle_404
app.error_handlers[500] = _handle_500


def main():
    u"""Starts the webapp2 framework."""

    app.run()


if __name__ == '__main__':
    main()

