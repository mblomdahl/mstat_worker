# _mstat\_worker_

The App Engine _worker_ module attempts to wrap web hooks aimed at

* performing maintenance and data sync operations
* enabling large NDB ingestions and update jobs, and
* providing auxiliary support for user-space interactions.


Base paths overview:

* `/backend/worker/<cmd>` – general-purpose web hooks, e.g. pull task deletions and long-running NDB queries
* `/backend/worker/rv_reassessment/<cmd>` – NDB ingestion of rateable value reassessments
* `/backend/worker/address_registry/<cmd>` – NDB ingestion of the PNS registry on Swedish postal addresses
* `/backend/worker/supplier_api_sync/<cmd>` – triggers initiating and handling the daily API consumption tasks
* `/backend/worker/email_sync/<cmd>` – IMAP sync hooks (used for reading emailed CSV data)
* `/backend/worker/stats_sync/<cmd>` – web hooks keeping system performance stats up-to-date
* `/backend/worker/bq_sync/<cmd>` – BigQuery sync routines
* `/backend/worker/sql_sync/<cmd>` – Cloud SQL sync routines
* `/backend/worker/memcache_sync/<cmd>` – web hooks maintaining memcached resources in sync


Please refer to project `cron.yaml` config for details on daily routines.


## 1. Prerequisites

Software:

* [Google App Engine Python SDK 1.9.2](https://developers.google.com/appengine/downloads)
* [Python 2.7.6](http://python.org/download/)
* [ecore 1.1](https://bitbucket.org/mblomdahl-ondemand/ecore)[^private]
* [mstat 1.06](https://bitbucket.org/mblomdahl-ondemand/mstat)[^private]
* [mstat_parser 1.06](https://bitbucket.org/mblomdahl/mstat_parser)

[^private]: Currently *not* publicly accessible.


## 2. Configuration

Locate `mstat` dev root and run

    $ git clone git@bitbucket.org:mblomdahl/mstat_worker.git


### 2.1. Module YAML Config

Add a `mstat_worker.yaml` configuration to the `mstat` root and set runtime environment to threadsafe Python 2.7 and
platform API version 1:

```
#!yaml
module: worker
api_version: 1
threadsafe: true
runtime: python27
```


Include the module's handlers/builtins configs:

```
#!yaml
includes:
- mstat_worker/handlers.yaml
- mstat_worker/builtins.yaml
```


Include required third-party libraries:

```
#!yaml
libraries:
  # Required for all request handling.
- name: webapp2
  version: latest

  # Required by the `sql_sync` component.
- name: MySQLdb
  version: latest

  # Required by a few models within the `ecore` library and the `sql_sync` component.
- name: jinja2
  version: latest

  # Required by the `ecore` library for IMAP connections.
- name: ssl
  version: latest
```


It's also highly recommended to exclude any unnecessary root resources from the module config, e.g.

```
#!yaml
skip_files:
  # Misc Application Logic
- ^(.*/)?mstat_parser/.*
- ^(.*/)?mstat_static_ui/.*
- ^(.*/)?mstat_front-end_api/.*
- ^(.*/)?mstat_static_resources/.*
- ^(.*/)?mstat_tce_userspace_stats/.*
- ^(.*/)?mstat_default/.*

  # Misc Resources
- ^(.*/)?playground/.*
- ^(.*/)?resources/public/.*
- ^(.*/)?templates/.*
- ^(.*/)?samples/.*

  # Configuration and Backup Files
- ^(.*/)?app\.yaml
- ^(.*/)?dispatch\.yaml
- ^(.*/)?mapreduce\.yaml
- ^(.*/)?index\.yaml
- ^(.*/)?cron\.yaml
- ^(.*/)?.*~
- ^(.*/)?.*\.py[co]
- ^(.*/)?.*/RCS/.*
- ^(.*/)?\..*
- ^(.*/)?.*\.bak$
```


### 2.2. Dispatch YAML Config

Add the following route to the `dispatch.yaml` configuration in the `mstat` root:

```
#!yaml
dispatch:
  # Route all invocations within the `mstat_worker` base path to the ``worker`` module.
- url: "*/backend/worker/*"
  module: worker
```


### 2.3. Cron Service YAML Config

Add any desired Cron init hooks to the `mstat` root `cron.yaml` configuration:

```
#!yaml
cron:
  # Check primary Gmail for new emails and perform ingestion to the `mstat_parser` module.
- description: Sync folder state and recover new emails from primary Gmail account
  url: /backend/worker/email_sync/init_imap_sync?email_address=<primary_gmail@host>&folders_category=shame&write_queue=slowparser
  schedule: <whenever>
  target: worker
  timezone: Europe/Stockholm

  # Perform daily consumption of the SFD web API.
- description: Perform daily processing of the SFD API
  url: /backend/worker/supplier_api_sync/process_sfd_api_data?refresh=recent&time_spacing=0&write_queue=slowparser&opt_write=true
  schedule: every day <whenever>
  target: worker
  timezone: Europe/Stockholm

  # Perform daily consumption of the SF web API.
- description: Perform daily processing of the SF API
  url: /backend/worker/supplier_api_sync/process_sf_api_data?refresh=recent&time_spacing=0&write_queue=slowparser&opt_write=true
  schedule: every day <whenever>
  target: worker
  timezone: Europe/Stockholm

  # Perform daily consumption of the LF web API.
- description: Perform daily processing of the LF API
  url: /backend/worker/supplier_api_sync/process_lf_api_data?refresh=recent&time_spacing=0&write_queue=slowparser&opt_write=true
  schedule: every day <whenever>
  target: worker
  timezone: Europe/Stockholm

  # Perform daily pre-allocation of BigQuery table resources (in case sync won't be triggered due to lack of out-of-sync data).
- description: Perform daily pre-allocation of BigQuery table resource
  url: /backend/worker/bq_sync/preallocate_bq_table_resources?kind=all
  schedule: every day <whenever>
  target: worker
  timezone: Europe/Stockholm

  # Locate entities tagged with the `sys_dirty` flag and refresh their corresponding BigQuery tables.
- description: Perform BigQuery synchronization
  url: /backend/worker/bq_sync/locate_bq_synchronization_targets?kind=all&refresh=sys_dirty
  schedule: <whenever>
  target: worker
  timezone: Europe/Stockholm

  # Perform daily synchronization of recently modified entities to the `core_kinds` Cloud SQL instance.
- description: Perform daily Cloud SQL synchronization
  url: /backend/worker/sql_sync/locate_sql_synchronization_targets
  schedule: every day <whenever>
  target: worker
  timezone: Europe/Stockholm

  # Locate _AbstractTransactionStats entities and keep metadata calculations up-to-date (preferably every morning).
- description: Source out-of-sync _AbstractTransactionStats records and recalculate stats
  url: /backend/worker/stats_sync/source_stats_sync_refresh_targets
  schedule: <whenever>
  target: worker
  timezone: Europe/Stockholm
```


## 3. Deployment

The module is deployed just like any other GAE module:

    $ appcfg.py --oauth2 update mstat_worker.yaml


## 4. Best Practices / Design Goals

* No errors should pass silently, ever.
    * All jobs are run through the App Engine TaskQueue – this assures any broken logic will accumulate as failing
    tasks in the application overview pages
    * All jobs submitted to the TaskQueue must be stateless – this assures that the tasks will be able to complete once
    the dev team deploys a bug fix
* Split any multi-step routines into single-step web hooks wherever possible
* Beware of fork bombs (may cause several thousand euros in damages)
* Always strive to find a "functional programming approach" to data processing within GAE


## 4.1. Basic Example

Basic example of a couple of web hooks in `/mstat_worker/root_handler.py`, enabling processing of massive NDB queries
(right-click and _Open Image in New Tab_ to make text readable):

![root_handler.py](https://bitbucket.org/mblomdahl/mstat_worker/downloads/worker_process_src.png)


## 5. Code Style

The project code style primarily relies on [PEP-8](http://google-styleguide.googlecode.com/svn/trunk/pyguide.html) and
[Google Python Style Guide](http://google-styleguide.googlecode.com/svn/trunk/pyguide.html).

Notable exceptions:

* Line-width: 120 ch.
* Imports convention

    No:

        from mstat_model.transactions import SfdRawDataEntry
        SfdRawDataEntry.get_default_sort_property()

    No:

        import mstat_model.transactions
        mstat_model.transactions.SfdRawDataEntry.get_default_sort_property()

    Yes:

        import mstat_model.transactions as transaction_model
        transaction_model.SfdRawDataEntry.get_default_sort_property()


Encoding: UTF-8

Line separator: Always LF (\n), never CRLF

Indentation: Always 4-space indents, never tabs (\t)


## 6. Known Issues

Many:

* No well-formed rollback behaviour for the `bq_sync` component when the multi-processing lock breaks unexpectedly.
* No satisfactory handling for BigQuery ingestion errors in the `bq_sync` component (failing tasks will simply stack up
  in the `bq-ingestions` queue).
* No efficient handling of recurring and persistent server errors from the _SF_ API in the `supplier_api_sync`
  component (will often experience downtime up to 4 days straight).
* The _daily_ BigQuery tables doesn't have any structured handling for expiration date and thus stacks up over time
  (`bq_sync` component).
* We lack support for making use of the new _description_ field in BigQuery (`bq_sync` component).
* We're stuck on `interactive` mode in all BigQuery API calls; all low-priority jobs should be run in `batch` mode
  whereever possible (`bq_sync` component).
* The `address_registry` component lacks support for well-formed handling of deprecated address registry entries.
* The `bq_sync` component still relies on Google's long deprecated
  [Cloud Storage Python API](https://developers.google.com/appengine/docs/python/googlestorage/).


## 7. Changelog


### Version 1.06

Changes:

* No Datastore entities will be updated until a successful BigQuery ingestion has been confirmed (`bq_sync` component).
* Improved multi-processing lock for target tables (`bq_sync` component).
* Added support for using the `NEWLINE_DELIMITED_JSON` import format (now default) when synchronizing to BigQuery
  (`bq_sync` component).
* Added support for creating placeholder tables for all months/days since _t(0)_ – even in cases where we're lacking
  data (`bq_sync` component).
* The `bq_sync` workflow will now automatically remove any Cloud Storage resources used during ingestion.


### Version 1.05

Changes:

* Added `opt_write` query parameter to the `supplier_api_sync` component.
* Added `write_queue` controls for the `memcache_sync` and `email_sync` components.
* Performed clean-up of sloppy docs and privileged details.
* Implemented the `address_registry` component.


### Version 1.04

Changes:

* Added the refactored `email_sync` component.
* Implemented web hooks within the `supplier_api_sync` component for the new suppliers _LF_ and _SF_.


## 8. Contact

For general inquiries, security issues and feature requests, contact lead developer
[Mats Blomdahl](mailto:mats.blomdahl@gmail.com).
