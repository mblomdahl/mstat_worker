# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.sql_sync
# Author:  Mats Blomdahl
# Version: 2014-02-18

u"""mstat_worker.sql_sync module

Usage:
    Invoke URLs at ``/backend/worker/sql_sync/<cmd>`` using the GAE TaskQueue or Cron Service.
"""

from sql_sync_handler import SqlSyncHandler
