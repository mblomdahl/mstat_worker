# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.sql_sync_process.SqlSyncHandler
# Author:  Mats Blomdahl, Olof Karlsson
# Version: 2014-03-27

import datetime
import logging
import MySQLdb

from google.appengine.api import taskqueue

from google.appengine.ext import ndb
from google.appengine.ext.ndb import Cursor

import ecore.formatting as formatting

import mstat_constants as constants

import mstat_generic_handler as generic_handler


_DEFAULT_PAGESIZE = 25
_DEFAULT_BATCH_SIZE = 250
_DEFAULT_SQL_INGESTION_BATCH_SIZE = 250

_DEFAULT_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_PROCESSING_QUEUE)
_DEFAULT_SOURCING_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_SOURCING_QUEUE)
_SQL_SYNC_QUEUE = taskqueue.Queue(constants.WORKER_SQL_SYNC_QUEUE)

_DEFAULT_TARGET_KINDS = ['TransactionCompositeEntry',
                         'SfRawDataEntry', 'LfRawDataEntry', 'CapitexRawDataEntry', 'SfdRawDataEntry',
                         'ScbRawDataEntry',
                         'AddressRegistryItem']


def _connect_sql_db():
    if constants.LOCAL:
        db = MySQLdb.connect(host='localhost',
                             user=constants.SQL_CORE_KINDS_USER,
                             passwd=constants.SQL_CORE_KINDS_PASSWORD,
                             db=constants.SQL_CORE_KINDS_DB)
    else:
        db = MySQLdb.connect(unix_socket=constants.SQL_CORE_KINDS_SOCKET,
                             user=constants.SQL_CORE_KINDS_USER,
                             db=constants.SQL_CORE_KINDS_DB)

    return db


def _resolve_kind(kind):
    import ecore.model.geocoding as ecore_geocoding_model
    import mstat_model.transactions as transaction_model

    if kind and kind not in _DEFAULT_TARGET_KINDS:
        raise AssertionError('Undefined Datastore kind \'%s\'.' % kind)

    # TODO(mats.blomdahl@gmail.com): Shame! Clean-up.
    try:
        kind_cls = eval('transaction_model.%s' % kind)
    except AttributeError:
        try:
            kind_cls = eval('ecore_geocoding_model.%s' % kind)
        except AttributeError:
            raise AssertionError('Cannot resolve Datastore kind \'%s\'.' % kind)
    finally:
        assert kind == kind_cls.__name__
        return kind_cls


def _get_kind_sys_modified_range(kind_cls, sys_modified_lower_lim):
    query = kind_cls.query(kind_cls.sys_modified >= sys_modified_lower_lim)
    dt_lower = query.order(kind_cls.sys_modified).get().sys_modified
    dt_upper = query.order(-kind_cls.sys_modified).get().sys_modified + datetime.timedelta(seconds=1)
    return dt_lower, dt_upper


class SqlSyncHandler(generic_handler.CommonTaskQueueInterface):
    u"""Handlers locating recently modified Datastore entities and performing synchronization with the corresponding
        Cloud SQL table.

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_worker/handlers.yaml` config).
    """

    # Interface: /backend/worker/sql_sync/locate_sql_synchronization_targets[?kind=<kind|all>[&batch_size=<batch_size>
    #     [&sql_ingestion_batch_size=<batch_size>[&if_modified_since=<datetime>]]]]
    def locate_sql_synchronization_targets(self):
        u"""Recursively dispatches ``source_entities_for_cloud_sql`` jobs to the TaskQueue for each 10 minute block
            since ``if_modified_since``.

        When the recursion ends, and the target kind is ``TransactionCompositeEntry``, the method will also dispatch
            a merge job to keep the product owner's `mstat_db.mstat_db` table up-to-date.

        Raises:
            AssertionError on invalid/undefined ``kind``, ``batch_size``, ``sql_ingestion_batch_size`` arguments.
            ValueError on malformed ``batch_size`` or ``sql_ingestion_batch_size`` arguments.
        """

        batch_size = int(self.request.get('batch_size', default_value=_DEFAULT_BATCH_SIZE))
        assert batch_size > 0

        sql_ingestion_batch_size = int(self.request.get('sql_ingestion_batch_size',
                                                        default_value=_DEFAULT_SQL_INGESTION_BATCH_SIZE))
        assert sql_ingestion_batch_size > 0

        try:
            if_modified_since = formatting.coerce_to_dt(self.request.get('if_modified_since'))
        except ValueError:
            if_modified_since = datetime.datetime.today() - datetime.timedelta(hours=48 + 2)
            logging.debug('[SqlSyncHandler.locate_sql_synchronization_targets] Valid \'if_modified_since\' input '
                          'missing, fallback set to %s.' % if_modified_since.isoformat())

        kind = self.request.get('kind', default_value='all')
        if kind == 'all':
            for kind_name in _DEFAULT_TARGET_KINDS:
                _DEFAULT_QUEUE.add(taskqueue.Task(
                    url='/backend/worker/sql_sync/locate_sql_synchronization_targets',
                    params={
                        'kind': kind_name,
                        'batch_size': batch_size,
                        'sql_ingestion_batch_size': sql_ingestion_batch_size,
                        'if_modified_since': if_modified_since.isoformat()
                    }
                ))

        else:
            try:
                sql_sync_t0 = formatting.coerce_to_dt(self.request.get('sql_sync_t0'))
            except ValueError:
                sql_sync_t0 = if_modified_since

            kind_cls = _resolve_kind(kind)

            try:
                modified_lower, modified_abs_upper = _get_kind_sys_modified_range(kind_cls, if_modified_since)
                modified_rel_upper, has_more = modified_lower + datetime.timedelta(minutes=10), True
                if modified_rel_upper > modified_abs_upper:
                    modified_rel_upper, has_more = modified_abs_upper, False
            except AttributeError as e:
                # Nothing to sync.
                logging.error('[SqlSyncHandler.locate_sql_synchronization_targets] AttributeError: \'%s\'' % e.message)
                return

            logging.info('[SqlSyncHandler.locate_sql_synchronization_targets] Querying NDB for entities of kind \'%s\' '
                         'in range %s--%s (sql_sync_t0: %s).'
                         % (kind, modified_lower.isoformat(), modified_rel_upper.isoformat(), sql_sync_t0.isoformat()))

            _SQL_SYNC_QUEUE.add(taskqueue.Task(url='/backend/worker/sql_sync/source_entities_for_cloud_sql',
                                               params={
                                                   'kind': kind,
                                                   'batch_size': batch_size,
                                                   'sql_ingestion_batch_size': sql_ingestion_batch_size,
                                                   'modified_lower_limit': modified_lower.isoformat(),
                                                   'modified_upper_limit': modified_rel_upper.isoformat()
                                               }))

            if has_more:
                _DEFAULT_QUEUE.add(taskqueue.Task(url='/backend/worker/sql_sync/locate_sql_synchronization_targets',
                                                  params={
                                                      'kind': kind,
                                                      'batch_size': batch_size,
                                                      'sql_ingestion_batch_size': sql_ingestion_batch_size,
                                                      'if_modified_since': modified_rel_upper.isoformat(),
                                                      'sql_sync_t0': sql_sync_t0.isoformat()
                                                  }))

            elif kind == 'TransactionCompositeEntry':
                # Merge from `core_kinds.transaction_composite_entry` to the product owner's `mstat_db.mstat_db` table.
                _SQL_SYNC_QUEUE.add(taskqueue.Task(url='/backend/worker/sql_sync/merge_core_kinds_tce_into_mstat_db',
                                                   params={'if_modified_since': sql_sync_t0.isoformat()},
                                                   countdown=60*4))

    # Interface: /backend/worker/sql_sync/source_entities_for_cloud_sql?modified_lower_limit=<datetime>
    #     &modified_upper_limit=<datetime>[&batch_size=<batch_size>][&sql_ingestion_batch_size=<batch_size>]
    #     [&cursor=<urlsafe_cursor>]
    def source_entities_for_cloud_sql(self):
        u"""Recursively reads Datastore entities and dispatches ``replace_into_cloud_sql`` jobs to the TaskQueue.

        Raises:
            AssertionError on invalid/missing ``kind`` argument.
            ValueError on invalid/malformed ``modified_lower_limit`` or ``modified_upper_limit`` arguments.
        """

        batch_size = int(self.request.get('batch_size', default_value=_DEFAULT_PAGESIZE))
        sql_ingestion_batch_size = int(self.request.get('sql_ingestion_batch_size',
                                                        default_value=_DEFAULT_SQL_INGESTION_BATCH_SIZE))

        cursor = Cursor(urlsafe=self.request.get('cursor', default_value=None))

        lower_dt_lim = formatting.coerce_to_dt(self.request.get('modified_lower_limit'))
        upper_dt_lim = formatting.coerce_to_dt(self.request.get('modified_upper_limit'))

        kind_cls = _resolve_kind(self.request.get('kind', default_value=None))
        target_sql_table = formatting.camel_to_lower_case(kind_cls.get_kind_name())

        query = kind_cls.query(ndb.AND(kind_cls.sys_modified >= lower_dt_lim, kind_cls.sys_modified < upper_dt_lim))
        entity_keys, cursor, has_more = query.fetch_page(batch_size, batch_size=batch_size, keys_only=True,
                                                         start_cursor=cursor)

        if any(entity_keys):
            _SQL_SYNC_QUEUE.add(taskqueue.Task(
                url='/backend/worker/sql_sync/replace_into_cloud_sql',
                params={
                    'kind': kind_cls.__name__,
                    'sql_ingestion_batch_size': sql_ingestion_batch_size,
                    'target_sql_table': target_sql_table,
                    'ndb_keys_urlsafe': formatting.tq_serialize([key.urlsafe() for key in entity_keys])
                }))

            if has_more:
                _DEFAULT_SOURCING_QUEUE.add(taskqueue.Task(
                    url='/backend/worker/sql_sync/source_entities_for_cloud_sql',
                    params={
                        'cursor': cursor.urlsafe(),
                        'batch_size': batch_size,
                        'sql_ingestion_batch_size': sql_ingestion_batch_size,
                        'modified_lower_limit': lower_dt_lim.isoformat(),
                        'modified_upper_limit': upper_dt_lim.isoformat(),
                        'kind': kind_cls.__name__
                    }))

    # Interface: /backend/worker/sql_sync/replace_into_cloud_sql?modified_lower_limit=<datetime>&modified_upper_limit=
    #     <datetime>[&batch_size=<batch_size>][&sql_ingestion_batch_size=<sql_batch_size>][&cursor=<urlsafe_cursor>]
    def replace_into_cloud_sql(self):
        u"""Performs a `REPLACE INTO` query on the target Cloud SQL table."""

        def _generate_sql_replace_query(table, columns):
            column_names, column_values = ', '.join(columns), ', '.join(['%s' for column in columns])
            return u"""REPLACE INTO %(table)s (%(column_names)s) VALUES (%(column_values)s)""" % locals()

        def _execute_replace(query, values):
            db = _connect_sql_db()
            db.cursor().executemany(query, values)
            db.commit()
            db.close()

        target_sql_table = self.request.get('target_sql_table', default_value=None)
        sql_ingestion_batch_size = int(self.request.get('sql_ingestion_batch_size',
                                                        default_value=_DEFAULT_SQL_INGESTION_BATCH_SIZE))

        kind = self.request.get('kind', default_value=None)
        kind_cls = _resolve_kind(kind)

        ndb_keys_urlsafe = formatting.tq_deserialize(self.request.get('ndb_keys_urlsafe', default_value=None))
        entity_keys, fields, table_rows, rows_count = \
            [ndb.Key(urlsafe=key) for key in ndb_keys_urlsafe], kind_cls._default_sql_fields(), [], 0

        while any(entity_keys):
            # TODO(mats.blomdahl@gmail.com): Shame! Use ctx.
            entities = ndb.get_multi(entity_keys[:_DEFAULT_BATCH_SIZE], deadline=3600)
            for entity in entities:
                table_rows.extend(entity.to_sql_tuple(fields)[0])
                if len(table_rows) >= sql_ingestion_batch_size:
                    _execute_replace(_generate_sql_replace_query(target_sql_table, fields), table_rows)
                    rows_count += len(table_rows)
                    table_rows = []
                    logging.info('[SqlSyncHandler.replace_into_cloud_sql] Synchronized %i entities of kind \'%s\' to '
                                 'Cloud SQL (target_sql_table: \'%s\').' % (rows_count, kind_cls.__name__,
                                                                            target_sql_table))
            else:
                if any(table_rows):
                    _execute_replace(_generate_sql_replace_query(target_sql_table, fields), table_rows)
                    rows_count += len(table_rows)
                    logging.info('[SqlSyncHandler.replace_into_cloud_sql] Synchronized %i entities of kind \'%s\' to '
                                 'Cloud SQL (target_sql_table: \'%s\').' % (rows_count, kind_cls.__name__,
                                                                            target_sql_table))

            entity_keys = entity_keys[_DEFAULT_BATCH_SIZE:]

    # Interface: /backend/worker/sql_sync/merge_core_kinds_tce_into_mstat_db?if_modified_since=<datetime>
    def merge_core_kinds_tce_into_mstat_db(self):
        u"""Merges the `core_kinds.transaction_composite_entry` table into the product owner's playground
            (`mstat_db.mstat_db`).

        Raises:
            ValueError on missing/invalid ``if_modified_since`` argument.
        """

        if_modified_since = formatting.coerce_to_dt(self.request.get('if_modified_since'))

        db = _connect_sql_db()
        read_write_ops = db.cursor().execute(u"""REPLACE INTO mstat_db.mstat_db
            SELECT
                transaction_id as ObjektId,
                real_estate_ad_publicized as Annonsdatum,
                contract_date as Kontraktsdatum,
                contract_price as Pris,
                date_of_possession as Tilltradesdatum,
                data_transfer_date as Overforingsdatum,
                mstat_restricted as KansligAffar,
                real_estate_ad_asking_price as Startpris,
                real_estate_designation as Fastbet,
                real_estate_taxation_code as Taxkod,
                real_estate_agent,
                real_estate_company,
                location_rt90_geopt,
                location_rt90_geopt_east as East,
                location_rt90_geopt_north as North,
                county,
                county_lkf,
                municipality,
                municipality_lkf,
                congregation,
                congregation_lkf as LKF,
                formatted_address as Gatuadress,
                route_name,
                street_number,
                postal_code,
                postal_town,
                apartment_number as LghNr,
                IF(1000 < baseline_year_of_assessment and baseline_year_of_assessment < 10000,
                   STR_TO_DATE(CONCAT(baseline_year_of_assessment, '-01-01'), '%%Y-%%m-%%d'),
                   NULL) as Taxar,
                baseline_rateable_value as Taxeringsvarde,
                IF(1000 < year_of_assessment and year_of_assessment < 10000,
                   STR_TO_DATE(CONCAT(year_of_assessment, '-01-01'), '%%Y-%%m-%%d'),
                   NULL) as year_of_assessment,
                rateable_value,
                price_by_rateable_value,
                price_per_sq_meter,
                type_of_housing as Boendeform,
                housing_category as Typ,
                housing_tenure as Upplatelseform,
                apartment_floor as Vaning,
                building_storeys as Vaningar,
                living_area as Boyta,
                plot_area as Tomtarea,
                number_of_rooms as Rum,
                elevator as Hiss,
                balcony as Balkong,
                new_production as NyProd,
                build_year as ByggAr,
                energy_performance,
                energy_rating,
                monthly_fee as Manavg,
                heating_included,
                housing_cooperative_name as BRF,
                housing_cooperative_registration_number as OrgNr,
                housing_cooperative_share as Andelstal,
                raw_type_of_housing,
                raw_housing_tenure,
                raw_real_estate_company_name,
                raw_real_estate_company_postal_town,
                raw_real_estate_company_franchisor,
                sys_raw_data_source as Kalla,
                sys_scb_invalidation_code as SCBBortfall,
                sys_modified,
                sys_created
            FROM
                core_kinds.transaction_composite_entry
            WHERE
                sys_modified >= '%s'
        """ % if_modified_since.isoformat())

        db.commit()
        db.close()

        logging.info('[SqlSyncHandler.merge_core_kinds_tce_into_mstat_db] Merged %i rows (roughly) from \'core_kinds'
                     '.transaction_composite_entry\' to \'mstat_db.mstat_db\' where \'sys_modified >= %s \'.'
                     % (read_write_ops, if_modified_since.isoformat()))
