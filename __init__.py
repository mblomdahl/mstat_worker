# -*- coding: utf-8 -*-
#
# Title:   mstat_worker
# Author:  Mats Blomdahl
# Version: 2014-02-01

"""mstat_worker module

Usage:
    Invoke URLs at ``/backend/worker/<cmd>`` using the GAE TaskQueue or Cron Service.
"""

from root_handler import RootHandler
