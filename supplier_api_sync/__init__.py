# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.supplier_api_sync
# Author:  Mats Blomdahl
# Version: 2014-02-01

u"""mstat_worker.supplier_api_sync module

Usage:
    Invoke URLs at ``/backend/worker/supplier_api_sync/<cmd>`` using the GAE TaskQueue or Cron Service.
"""

from supplier_api_sync_handler import SupplierApiSyncHandler
