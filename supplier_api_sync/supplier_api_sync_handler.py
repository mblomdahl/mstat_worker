# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.supplier_api_sync_process.SupplierApiSyncHandler
# Author:  Mats Blomdahl
# Version: 2014-02-28

import datetime
import pprint
import logging
import random

from google.appengine.api import taskqueue
import json

import ecore.formatting as formatting

import mstat_constants as constants

import mstat_generic_handler as generic_handler

import api_fetcher


_DEFAULT_PAGESIZE = 25

_DEFAULT_SOURCING_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_SOURCING_QUEUE)
_DEFAULT_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_PROCESSING_QUEUE)
_SFD_XML_PARSER_QUEUE = taskqueue.Queue(constants.PARSER_SFD_XML_API_SOURCING_QUEUE)
_LF_JSON_PARSER_QUEUE = taskqueue.Queue(constants.PARSER_LF_JSON_API_SOURCING_QUEUE)
_SF_JSON_PARSER_QUEUE = taskqueue.Queue(constants.PARSER_SF_JSON_API_SOURCING_QUEUE)


class SupplierApiSyncHandler(generic_handler.CommonTaskQueueInterface):
    u"""Handlers dispatching supplier web API consumption tasks.

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_worker/handlers.yaml` config).
    """

    # Interface: /backend/worker/supplier_api_sync/process_legacy_lf_api_data
    def process_legacy_lf_api_data(self):
        u"""Dispatches API consumption jobs to the GAE TaskQueue for raw data supplier LF.

        Raises:
            AssertionError on invalid request parameters for `time_spacing`, `refresh`, `opt_write` or `write_queue`.
        """

        _LF_T0 = datetime.date(1990, 1, 1)
        _Y2K = datetime.date(2000, 1, 1)

        def _generate_longterm_sync_intervals(upper_date_lim):
            pre_2000_mx = formatting.date_range_sub_intervals(_LF_T0, _Y2K, days_dx=31, output_date_type=datetime.date)

            post_2000_wx = formatting.date_range_sub_intervals(_Y2K, upper_date_lim, 7, output_date_type=datetime.date)

            return random.sample(pre_2000_mx, 2) + random.sample(post_2000_wx, 8)

        start_date = self.request.get('start_date', default_value=None)
        end_date = self.request.get('end_date', default_value=None)

        time_spacing = int(self.request.get('time_spacing', default_value='0'))
        assert time_spacing >= 0

        opt_write = self.request.get('opt_write', default_value='false').lower()
        assert opt_write in ['false', 'true']

        write_queue = self.request.get('write_queue', default_value=constants.PARSER_JSON_PROCESSING_QUEUE)
        assert write_queue in [constants.PARSER_JSON_PROCESSING_QUEUE, constants.PARSER_SLOW_PROCESSING_QUEUE]

        longterm_sync_intervals = []

        if start_date is None and end_date is None:
            date_today = datetime.date.today()

            refresh = self.request.get('refresh', default_value='recent').lower()
            assert refresh in ['full', 'recent']

            if refresh == 'recent':
                start_date = formatting.dt_monthdelta(date_today, -5)
                longterm_sync_intervals = _generate_longterm_sync_intervals(start_date)
                end_date = formatting.dt_monthdelta(start_date, +2)
            else:  # Full refresh.
                start_date = _LF_T0
                end_date = date_today + datetime.timedelta(days=1)  # formatting.dt_monthdelta(date_today, +1)

        else:
            start_date = formatting.coerce_to_date(start_date)
            end_date = formatting.coerce_to_date(end_date)

        target_intervals = longterm_sync_intervals + \
            random.sample(formatting.date_range_sub_intervals(start_date, end_date, 1,
                                                              output_date_type=datetime.date), 7)

        countdown = time_spacing
        for start_date, end_date in target_intervals:
            start_date, end_date = start_date.isoformat(), end_date.isoformat()
            logging.debug('[SupplierApiSyncProcess.process_legacy_lf_api_data] Queueing LF raw data ingestions in date '
                          'range %s--%s (\'countdown\'=%i s)...' % (start_date, end_date, countdown))
            _LF_JSON_PARSER_QUEUE.add(taskqueue.Task(
                url='/backend/jsonparser/process_lf_json_api',
                params={
                    'start_date': start_date,
                    'end_date': end_date,
                    'write_queue': write_queue,
                    'opt_write': opt_write
                },
                countdown=countdown
            ))
            countdown += time_spacing

    # Interface: /backend/worker/supplier_api_sync/process_legacy_sf_api_data
    def process_legacy_sf_api_data(self):
        u"""Dispatches API consumption jobs to the GAE TaskQueue for raw data supplier SF.

        Raises:
            AssertionError on invalid request parameters for `time_spacing`, `refresh`, `opt_write` or `write_queue`.
        """

        _SF_T0 = datetime.date(1990, 1, 1)
        _MSTAT_T0 = datetime.date(2005, 1, 1)
        _Y2K = datetime.date(2000, 1, 1)

        def _generate_longterm_sync_intervals(upper_date_lim):
            pre_2000_mx = formatting.date_range_sub_intervals(_SF_T0, _Y2K, days_dx=31, output_date_type=datetime.date)

            pre_2005_wx = formatting.date_range_sub_intervals(_Y2K, _MSTAT_T0, 7, output_date_type=datetime.date)

            post_2005_dx = formatting.date_range_sub_intervals(_MSTAT_T0, upper_date_lim, 1,
                                                               output_date_type=datetime.date)

            return random.sample(pre_2000_mx + pre_2005_wx, 1) + random.sample(post_2005_dx, 12)

        start_date = self.request.get('start_date', default_value=None)
        end_date = self.request.get('end_date', default_value=None)

        time_spacing = int(self.request.get('time_spacing', default_value='0'))
        assert time_spacing >= 0

        opt_write = self.request.get('opt_write', default_value='false').lower()
        assert opt_write in ['false', 'true']

        write_queue = self.request.get('write_queue', default_value=constants.PARSER_JSON_PROCESSING_QUEUE)
        assert write_queue in [constants.PARSER_JSON_PROCESSING_QUEUE, constants.PARSER_SLOW_PROCESSING_QUEUE]

        longterm_sync_intervals = []

        if start_date is None and end_date is None:
            date_today = datetime.date.today()

            refresh = self.request.get('refresh', default_value='recent').lower()
            assert refresh in ['full', 'recent']

            if refresh == 'recent':
                start_date = formatting.dt_monthdelta(date_today, -5)
                longterm_sync_intervals.extend(_generate_longterm_sync_intervals(start_date))
                end_date = formatting.dt_monthdelta(start_date, +2)
            else:  # Full refresh.
                start_date = _SF_T0
                end_date = date_today + datetime.timedelta(days=1)  # formatting.dt_monthdelta(date_today, +1)

        else:
            start_date, end_date = formatting.coerce_to_date(start_date), formatting.coerce_to_date(end_date)

        target_intervals = longterm_sync_intervals + \
            random.sample(formatting.date_range_sub_intervals(start_date, end_date, 1,
                                                              output_date_type=datetime.date), 7)

        countdown = time_spacing
        for start_date, end_date in target_intervals:
            start_date, end_date = start_date.isoformat(), end_date.isoformat()
            countdown += time_spacing
            logging.debug('[SupplierApiSyncProcess.process_legacy_sf_api_data] Queueing SF raw data ingestions in date '
                          'range %s--%s (\'countdown\'=%i s)...' % (start_date, end_date, countdown))
            _SF_JSON_PARSER_QUEUE.add(taskqueue.Task(
                url='/backend/jsonparser/process_sf_json_api',
                params={
                    'start_date': start_date,
                    'end_date': end_date,
                    'write_queue': write_queue,
                    'opt_write': opt_write
                },
                countdown=countdown
            ))

    # Interface: /backend/worker/supplier_api_sync/process_mspecs_api_data
    def process_mspecs_api_data(self):
        u"""Dispatches API consumption jobs to the GAE TaskQueue for raw data supplier Mspecs.

        Raises:
            NotImplementedError.
        """

        raise NotImplementedError('Mspecs API consumption routine not implemented.')

    # Interface: /backend/worker/supplier_api_sync/process_fasad_api_data
    def process_fasad_api_data(self):
        u"""Dispatches API consumption jobs to the GAE TaskQueue for raw data supplier Fasad.

        Raises:
            NotImplementedError.
        """

        raise NotImplementedError('Fasad API consumption routine not implemented.')

    # Interface: /backend/worker/supplier_api_sync/process_sfd_api_data
    def process_sfd_api_data(self):
        u"""Dispatches API consumption jobs to the GAE TaskQueue for raw data supplier SFD.

        Raises:
            AssertionError on invalid request parameters for `time_spacing`, `refresh`, `opt_write` or `write_queue`.
        """

        _SFD_T0 = formatting.dt_monthdelta(datetime.date.today(), -3).replace(day=1)

        def _generate_longterm_sync_intervals(upper_date_lim):
            lower_date_lim = _SFD_T0 + datetime.timedelta(days=7)
            last_days = formatting.date_range_sub_intervals(_SFD_T0, lower_date_lim, 1, output_date_type=datetime.date)

            mid_interval = formatting.date_range_sub_intervals(lower_date_lim, upper_date_lim, 1,
                                                               output_date_type=datetime.date, reversed=True)
            every_3d = [mid_interval[i] for i in range(len(mid_interval)) if i % 3 == 0]

            return last_days + every_3d

        start_date = self.request.get('start_date', default_value=None)
        end_date = self.request.get('end_date', default_value=None)

        time_spacing = int(self.request.get('time_spacing', default_value='0'))
        assert time_spacing >= 0

        opt_write = self.request.get('opt_write', default_value='false').lower()
        assert opt_write in ['false', 'true']

        write_queue = self.request.get('write_queue', default_value=constants.PARSER_JSON_PROCESSING_QUEUE)
        assert write_queue in [constants.PARSER_JSON_PROCESSING_QUEUE, constants.PARSER_SLOW_PROCESSING_QUEUE]

        longterm_sync_intervals = []

        if start_date is None and end_date is None:
            date_today = datetime.date.today()

            refresh = self.request.get('refresh', default_value='recent').lower()
            assert refresh in ['recent', 'full']

            if refresh == 'recent':
                start_date = date_today - datetime.timedelta(days=14)
                longterm_sync_intervals = _generate_longterm_sync_intervals(start_date)
            else:  # Full refresh.
                start_date = _SFD_T0
            end_date = date_today + datetime.timedelta(days=1)

        else:
            start_date = formatting.coerce_to_date(start_date)
            end_date = formatting.coerce_to_date(end_date)

        target_intervals = longterm_sync_intervals + formatting.date_range_sub_intervals(start_date, end_date, 1,
                                                                                         output_date_type=datetime.date)
        countdown = time_spacing
        for start_date, end_date in target_intervals:
            countdown += time_spacing
            start_date, end_date = start_date.isoformat(), end_date.isoformat()
            logging.debug('[SupplierApiSyncProcess.process_sfd_api_data] Queueing SFD raw data ingestion for date range'
                          ' %s--%s (\'countdown\'=%i s)...' % (start_date, end_date, countdown))
            _SFD_XML_PARSER_QUEUE.add(taskqueue.Task(
                url='/backend/worker/supplier_api_sync/load_sfd_api_data',
                params={
                    'start_date': start_date,
                    'end_date': end_date,
                    'write_queue': write_queue,
                    'opt_write': opt_write
                },
                countdown=countdown
            ))

    # Interface: /backend/worker/supplier_api_sync/process_sf_api_data
    def process_sf_api_data(self):
        u"""Dispatches API consumption jobs to the GAE TaskQueue for raw data supplier SF.

        Raises:
            AssertionError on invalid request parameters for `time_spacing`, `refresh`, `opt_write` or `write_queue`.
        """

        _SF_T0 = datetime.date(1990, 1, 1)

        start_date = self.request.get('start_date', default_value=None)
        end_date = self.request.get('end_date', default_value=None)

        time_spacing = int(self.request.get('time_spacing', default_value='0'))
        assert time_spacing >= 0

        opt_write = self.request.get('opt_write', default_value='false').lower()
        assert opt_write in ['false', 'true']

        write_queue = self.request.get('write_queue', default_value=constants.PARSER_JSON_PROCESSING_QUEUE)
        assert write_queue in [constants.PARSER_JSON_PROCESSING_QUEUE, constants.PARSER_SLOW_PROCESSING_QUEUE]

        if start_date is None and end_date is None:
            end_date = formatting.utc_to_swe_dt(datetime.datetime.today())
            logging.info('swe_dt_today: %s' % end_date.isoformat())
            end_date = formatting.coerce_to_date(end_date)

            refresh = self.request.get('refresh', default_value='recent').lower()
            assert refresh in ['full', 'recent']

            start_date = end_date - datetime.timedelta(days=2) if refresh == 'recent' else _SF_T0

        else:
            start_date, end_date = formatting.coerce_to_date(start_date), formatting.coerce_to_date(end_date)

        target_intervals = formatting.date_range_sub_intervals(start_date, end_date, 1, output_date_type=datetime.date)
        countdown = time_spacing
        for start_date, end_date in target_intervals:
            countdown += time_spacing
            start_date, end_date = start_date.isoformat(), end_date.isoformat()
            logging.info('[SupplierApiSyncProcess.process_sf_api_data] Queueing SF raw data ingestions for \'AktTid\' '
                         'updates in range range %s--%s (\'countdown\'=%i s)...' % (start_date, end_date, countdown))
            _SF_JSON_PARSER_QUEUE.add(taskqueue.Task(
                url='/backend/worker/supplier_api_sync/load_sf_api_data',
                params={
                    'start_date': start_date,
                    'end_date': end_date,
                    'write_queue': write_queue,
                    'opt_write': opt_write
                },
                countdown=countdown
            ))

    # Interface: /backend/worker/supplier_api_sync/process_lf_api_data
    def process_lf_api_data(self):
        u"""Dispatches API consumption jobs to the GAE TaskQueue for raw data supplier LF.

        Raises:
            AssertionError on invalid request parameters for `time_spacing`, `refresh`, `opt_write` or `write_queue`.
        """

        _LF_T0 = datetime.date(1990, 1, 1)

        start_date = self.request.get('start_date', default_value=None)
        end_date = self.request.get('end_date', default_value=None)

        time_spacing = int(self.request.get('time_spacing', default_value='0'))
        assert time_spacing >= 0

        opt_write = self.request.get('opt_write', default_value='false').lower()
        assert opt_write in ['false', 'true']

        write_queue = self.request.get('write_queue', default_value=constants.PARSER_JSON_PROCESSING_QUEUE)
        assert write_queue in [constants.PARSER_JSON_PROCESSING_QUEUE, constants.PARSER_SLOW_PROCESSING_QUEUE]

        if start_date is None and end_date is None:
            end_date = formatting.utc_to_swe_dt(datetime.datetime.today())
            logging.info('swe_dt_today: %s' % end_date.isoformat())
            end_date = formatting.coerce_to_date(end_date)

            refresh = self.request.get('refresh', default_value='recent').lower()
            assert refresh in ['full', 'recent']

            start_date = end_date - datetime.timedelta(days=2) if refresh == 'recent' else _LF_T0

        else:
            start_date, end_date = formatting.coerce_to_date(start_date), formatting.coerce_to_date(end_date)

        target_intervals = formatting.date_range_sub_intervals(start_date, end_date, 1, output_date_type=datetime.date)
        countdown = time_spacing
        for start_date, end_date in target_intervals:
            countdown += time_spacing
            start_date, end_date = start_date.isoformat(), end_date.isoformat()
            logging.info('[SupplierApiSyncProcess.process_lf_api_data] Queueing LF raw data ingestions for \'AktTid\' '
                         'updates in range range %s--%s (\'countdown\'=%i s)...' % (start_date, end_date, countdown))
            _LF_JSON_PARSER_QUEUE.add(taskqueue.Task(
                url='/backend/worker/supplier_api_sync/load_lf_api_data',
                params={
                    'start_date': start_date,
                    'end_date': end_date,
                    'write_queue': write_queue,
                    'opt_write': opt_write
                },
                countdown=countdown
            ))

    # Interface: /backend/worker/supplier_api_sync/load_sfd_api_data
    def load_sfd_api_data(self):
        u"""Loads API data from supplier SFD and dispatches update jobs the App Engine `parser` module.

        Raises:
            AssertionError on invalid request parameters for `write_queue` or `opt_write`.
        """

        start_date = formatting.coerce_to_date(self.request.get('start_date', default_value=None))
        end_date = formatting.coerce_to_date(self.request.get('end_date', default_value=None))

        write_queue = self.request.get('write_queue', default_value=None)
        assert write_queue is not None
        write_queue = taskqueue.Queue(write_queue)

        opt_write = self.request.get('opt_write', default_value=None)
        assert opt_write in ('true', 'false')

        last_submitted = {'sys_last_submitted': datetime.datetime.today().isoformat().split('.')[0]}
        for api_output in api_fetcher.SfdApiFetcher(start_date, end_date, apply_dict=last_submitted):
            logging.debug('[SupplierApiSyncProcess.load_sfd_api_data] Submitting dataset to \'/backend/jsonparser/'
                          'put_sfd_raw_data_entry\' (item: \'%s\').' % pprint.pformat(api_output))
            write_queue.add(taskqueue.Task(
                url='/backend/jsonparser/put_sfd_raw_data_entry',
                params={'opt_write': opt_write, 'task_data': json.dumps(api_output)}
            ))

    # Interface: /backend/worker/supplier_api_sync/load_sf_api_data
    def load_sf_api_data(self):
        u"""Loads API data from supplier SF and dispatches update jobs the App Engine `parser` module.

        Raises:
            AssertionError on invalid request parameters for `write_queue` or `opt_write`.
        """

        start_date = formatting.coerce_to_date(self.request.get('start_date', default_value=None))
        end_date = formatting.coerce_to_date(self.request.get('end_date', default_value=None))

        write_queue = self.request.get('write_queue', default_value=None)
        assert write_queue is not None
        write_queue = taskqueue.Queue(write_queue)

        opt_write = self.request.get('opt_write', default_value=None)
        assert opt_write in ('true', 'false')

        last_submitted = {'sys_last_submitted': datetime.datetime.today().isoformat().split('.')[0]}
        for api_output in api_fetcher.SfApiFetcher(start_date, end_date, apply_dict=last_submitted):
            logging.debug('[SupplierApiSyncProcess.load_sf_api_data] Submitting dataset to \'/backend/jsonparser/'
                          'put_sf_raw_data_entry\' (item: \'%s\').' % pprint.pformat(api_output))
            write_queue.add(taskqueue.Task(
                url='/backend/jsonparser/put_sf_raw_data_entry',
                params={'opt_write': opt_write, 'task_data': json.dumps(api_output)}
            ))

    # Interface: /backend/worker/supplier_api_sync/load_lf_api_data
    def load_lf_api_data(self):
        u"""Loads API data from supplier LF and dispatches update jobs the App Engine `parser` module.

        Raises:
            AssertionError on invalid request parameters for `write_queue` or `opt_write`.
        """

        start_date = formatting.coerce_to_date(self.request.get('start_date', default_value=None))
        end_date = formatting.coerce_to_date(self.request.get('end_date', default_value=None))

        write_queue = self.request.get('write_queue', default_value=None)
        assert write_queue is not None
        write_queue = taskqueue.Queue(write_queue)

        opt_write = self.request.get('opt_write', default_value=None)
        assert opt_write in ('true', 'false')

        last_submitted = {'sys_last_submitted': datetime.datetime.today().isoformat().split('.')[0]}
        for api_output in api_fetcher.LfApiFetcher(start_date, end_date, apply_dict=last_submitted):
            logging.debug('[SupplierApiSyncProcess.load_lf_api_data] Submitting dataset to \'/backend/jsonparser/'
                          'put_lf_raw_data_entry\' (item: \'%s\').' % pprint.pformat(api_output))
            write_queue.add(taskqueue.Task(
                url='/backend/jsonparser/put_lf_raw_data_entry',
                params={'opt_write': opt_write, 'task_data': json.dumps(api_output)}
            ))

