# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.supplier_api_sync.api_fetcher
# Author:  Mats Blomdahl
# Version: 2014-02-27

import logging
import json
import re
import datetime

from lxml import etree

from google.appengine.api import urlfetch

import ecore.dateutil as dateutil

import mstat_model.transactions as transaction_model

import mstat_constants as constants


_DEFAULT_RETRIES = 9
_DEFAULT_TIMEOUT = 60


class ServerErrorException(Exception):
    u"""Generic server error."""


class ServerTimeoutException(Exception):
    u"""Server timeout error."""


class _AbstractApiFetcher(object):
    u"""Abstract utility class performing API ingestions from the product owner's raw data suppliers.

    Usage:
        >>> for api_output in _AbstractApiFetcher(u'/Users/kitty/secret_files/api_credentials.json',
        ...                                       datetime.date(2013, 01, 24), datetime.date(2013, 01, 27)):
        ...     pass  # Do stuff with output in interval [2013-01-24T00:00:00, 2013-01-27T00:00:00).
    """

    def __init__(self, credentials_path, start_date, end_date, apply_dict=None):
        u"""Initializes fetcher details.

        Args:
            credentials_path: Full file path to a JSON object containing ``access_key`` and ``url_tpl``.
            start_date: Query range lower limit, a date/datetime instance.
            end_date: Query range upper limit, a date/datetime instance.
            apply_dict: Custom dict to apply on each item in API output.
        """

        self._apply_dict = apply_dict
        self._results = None

        url_tpl, access_key = self._get_credentials(credentials_path)
        self.request_url = self._format_url(url_tpl, access_key, start_date, end_date)

    @staticmethod
    def _get_credentials(file_path):
        with file(file_path, 'rb') as f:
            credentials = json.loads(f.read())
            f.close()
        return credentials['url_tpl'], credentials['access_key']

    @staticmethod
    def _format_url(url_tpl, access_key, start_date, end_date):
        u"""Formats the request URL."""

        start_date, end_date = start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')
        return url_tpl % locals()

    def _invoke_fetch_call(self, url, retries=None):
        u"""Invokes API calls using the App Engine Urlfetch API.

        Args:
            url: Target URL.
            retries: Number of retries to attempt on ``urlfetch.DeadlineExceededError``.
        """

        logging.debug('[_AbstractApiFetcher._invoke_fetch_call] Loading \'%s\'...' % url)
        retries = _DEFAULT_RETRIES if retries is None else retries
        for i in range(retries + 1):
            try:
                result = urlfetch.fetch(url=url, deadline=_DEFAULT_TIMEOUT)
                if result.status_code != 200:
                    raise ServerErrorException('Fetch call returned status code %i.' % result.status_code)
                else:
                    self._results = self._post_process_api_response(result.content)
                    break
            except urlfetch.DeadlineExceededError as e:
                logging.error(
                    '[_AbstractApiFetcher._invoke_fetch_call] Raised DeadlineExceededError: \'%s\'' % e.message)
        else:
            raise ServerTimeoutException('API fetch failed after %i retries.' % retries)

    @staticmethod
    def _post_process_api_response(api_response):
        u"""Abstract method for post-processing API output.

        Args:
            api_response: Raw HTTP response body.

        Returns:
            A list of JSON serializable dicts.
        """

        raise NotImplementedError('\'_post_process_api_response\' override missing.')

    def __iter__(self):
        u"""Yields API output.

        Returns:
            A JSON serializable dict.

        Raises:
            ServerErrorException on server responses with status code other than 200.
            ServerTimeoutException on repeated timeout errors.
        """

        if self._results is None:
            self._invoke_fetch_call(self.request_url)

        for item in self._results:
            if self._apply_dict:
                item.update(self._apply_dict)
            yield item


class SfApiFetcher(_AbstractApiFetcher):
    u"""Utility class performing API ingestion from the SF web API.

    Usage:
        >>> for api_output in SfApiFetcher(datetime.date(2014, 01, 10), datetime.date(2014, 01, 12),
        ...                                apply_dict={'sys_last_submitted': datetime.datetime.today().isoformat()}):
        ...     pass  # Do stuff with output in interval [2014-01-10T00:00:00, 2014-01-12T00:00:00).
    """

    def __init__(self, start_date, end_date, apply_dict=None):
        u"""Initializes a fetcher for the SF JSON API."""

        super(SfApiFetcher, self).__init__('%s/resources/private/json/sf_api.json' % constants.APP_ROOT_PATH,
                                           start_date, end_date, apply_dict=apply_dict)

    @staticmethod
    def _post_process_api_response(api_response):

        _DT_PROPERTY_NAMES = transaction_model.SfRawDataEntry.get_dt_properties()

        properties = json.loads(api_response)['properties']
        for item in properties:
            for name in _DT_PROPERTY_NAMES:
                if item.get(name, None):
                    item[name] = item[name].split('.')[0]
        return properties


class LfApiFetcher(_AbstractApiFetcher):
    u"""Utility class performing API ingestion from the LF web API.

    Usage:
        >>> for api_output in LfApiFetcher(datetime.date(2013, 12, 06), datetime.date(2013, 12, 10),
        ...                                apply_dict={'sys_last_submitted': datetime.datetime.today().isoformat()}):
        ...     pass  # Do stuff with output in interval [2013-12-06T00:00:00, 2013-12-10T00:00:00).
    """

    def __init__(self, start_date, end_date, apply_dict=None):
        u"""Initializes a fetcher for the LF JSON API."""

        super(LfApiFetcher, self).__init__('%s/resources/private/json/lf_api.json' % constants.APP_ROOT_PATH,
                                           start_date, end_date, apply_dict=apply_dict)

    @staticmethod
    def _post_process_api_response(api_response):

        _DT_PROPERTY_NAMES = transaction_model.LfRawDataEntry.get_dt_properties()

        properties = json.loads(api_response)['properties']
        for item in properties:
            for name in _DT_PROPERTY_NAMES:
                if item.get(name, None):
                    item[name] = item[name].split('.')[0]
        return properties


class SfdApiFetcher(_AbstractApiFetcher):
    u"""Utility class performing API ingestion from the SFD web API.

    Usage:
        >>> for api_output in SfdApiFetcher(datetime.date(2013, 12, 06), datetime.date(2013, 12, 10),
        ...                                 apply_dict={'sys_last_submitted': datetime.datetime.today().isoformat()}):
        ...     pass  # Do stuff with output in interval [2013-12-06T00:00:00, 2013-12-10T00:00:00).
    """

    def __init__(self, start_date, end_date, apply_dict=None):
        u"""Initializes a fetcher for the SFD XML API."""

        end_date -= datetime.timedelta(days=1)  # Since the SFD web API uses a closed interval for target date.

        super(SfdApiFetcher, self).__init__('%s/resources/private/json/sfd_api.json' % constants.APP_ROOT_PATH,
                                            start_date, end_date, apply_dict=apply_dict)

    @staticmethod
    def _post_process_api_response(api_response):

        def _strip_int(val):
            val = val.strip()
            match = re.match(r'\d+', val)
            return int(val[match.start():match.end()]) if match else None

        def _rt90_sanity_check(x, y):
            try:
                if not isinstance(x, (str, unicode)):
                    raise TypeError('RT90 X-coordinate cannot be type %r.' % type(x))
                elif not isinstance(y, (str, unicode)):
                    raise TypeError('RT90 Y-coordinate cannot be type %r.' % type(x))
                else:
                    x, y = _strip_int(x), _strip_int(y)
                    return (x, y) if x > y else (y, x)
            except TypeError as e:
                logging.error(
                    '[SfdApiFetcher._post_process_api_response._rt90_sanity_check] Raised TypeError: %r' % e.message)
                return None, None

        xml_data, json_data = etree.fromstring(api_response.decode('iso-8859-1')), []
        for el in xml_data:
            if el.tag == 'Objekt':
                json_obj = {}
                for field in el:
                    if field.tag in ['AktTid', 'Avtalsdag', 'Tilltrdag', 'Annonstid', 'AnnonsTid']:
                        dtz_field = dateutil.parser.parse(field.text.split('+0')[0])  # Strip TZ-info.
                        dtz_field = dtz_field.isoformat().split('.')[0]  # Strip sub-second data.
                        if field.tag == 'AnnonsTid':
                            json_obj['Annonstid'] = dtz_field
                        else:
                            json_obj[field.tag] = dtz_field
                    else:
                        json_obj[field.tag] = field.text

                json_obj['X'], json_obj['Y'] = _rt90_sanity_check(json_obj['X'], json_obj['Y'])

                json_data.append(json_obj)

        return json_data



