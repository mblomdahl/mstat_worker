# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.rv_reassessment
# Author:  Mats Blomdahl
# Version: 2014-02-01

u"""mstat_worker.rv_reassessment module

Usage:
    Invoke URLs at ``/backend/worker/rv_reassessment/<cmd>`` using the GAE TaskQueue or Cron Service.
"""

from rv_reassessment_handler import RvReassessmentHandler
