# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.rv_reassessment.RvReassessmentHandler
# Author:  Mats Blomdahl
# Version: 2013-09-09

import json
import logging
import pprint

from google.appengine.api import taskqueue

import ecore.model.transactions as ecore_transactions

import mstat_constants as constants

import mstat_generic_handler as generic_handler


_DEFAULT_PAGESIZE = 25

_DEFAULT_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_PROCESSING_QUEUE)


class RvReassessmentHandler(generic_handler.CommonTaskQueueInterface):
    u"""Handlers performing Datastore ingestion of rateable value reassessment factors.

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_worker/handlers.yaml` config).
    """

    # Interface: /backend/worker/rv_reassessment/source_rv_translation_entries
    def source_rv_translation_entries(self):
        u"""Queues NDB writes for predefined rateable value translations.

        Raises:
            AssertionError on invalid request parameter for `batch_size`.
        """

        import rv_translation_table

        batch_size = int(self.request.get('batch_size', default_value=_DEFAULT_PAGESIZE))
        assert batch_size > 0

        translation_table, tq_batches = rv_translation_table.to_dict(), [list()]
        for key_id, properties in translation_table.iteritems():
            if len(tq_batches[-1]) >= batch_size:
                tq_batches.append([])
            tq_batches[-1].append(dict(id=key_id, **properties))

        total_queued, len_translations = 0, len(translation_table)
        for batch in tq_batches:
            _DEFAULT_QUEUE.add(taskqueue.Task(
                url='/backend/worker/rv_reassessment/process_rv_translation_entries',
                payload=json.dumps({'rv_translations': batch})
            ))
            total_queued += len(batch)
            logging.debug(
                '[RvReassessmentProcess.source_rv_translation_entries] total_queued: %i / len(translation_table): %i'
                % (total_queued, len_translations))

    # Interface: /backend/worker/rv_reassessment/process_rv_translation_entries
    def process_rv_translation_entries(self):
        u"""Writes rateable value translation entities to NDB."""

        translation_kind = ecore_transactions.RateableValueTranslation

        for translation in json.loads(self.request.body)['rv_translations']:
            logging.debug('[RvReassessmentProcess.process_rv_translation_entries] Writing RV translation: \'%s\''
                          % pprint.pformat(translation))
            # TODO(mats.blomdahl@gmail.com): Shame! Use ctx.
            translation_kind(**translation).put(deadline=3600)
