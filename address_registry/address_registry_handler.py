# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.address_registry.AddressRegistryHandler
# Author:  Mats Blomdahl
# Version: 2014-02-28

import logging
import pprint

from google.appengine.api import taskqueue

import ecore.formatting as formatting

import ecore.constants as ecore_constants
import ecore.model.geocoding as ecore_geocoding

import mstat_constants as constants

import mstat_generic_handler as generic_handler


_DEFAULT_PAGESIZE = 25

_ADDRESS_REGISTRY_DEPRECATION_QUEUE = taskqueue.Queue('address-registry-deprecation-2014')
_DEFAULT_SOURCING_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_SOURCING_QUEUE)
_DEFAULT_QUEUE = taskqueue.Queue(constants.WORKER_DEFAULT_PROCESSING_QUEUE)

_SRC_FILE_BASE_PATH = '%s/resources/private/csv/' % constants.APP_ROOT_PATH


class AddressRegistryHandler(generic_handler.CommonTaskQueueInterface):
    u"""Handlers performing Datastore ingestion of the Svensk Postnummerservice address registry.

    Security: All handlers are secured by App Engine's built-in authentication (`/mstat_worker/handlers.yaml` config).
    """

    # Interface: /backend/worker/address_registry/source_address_registry_items
    def source_address_registry_items(self):
        u"""Queues NDB write ops for the Swedish postal address registry.

        Raises:
            AssertionError on invalid request parameter for `county_lkf`.
        """

        filename = self.request.get('filename', default_value='address_registry_2013-09.zip')
        limit = self.request.get('limit', default_value=None)
        county_lkf = self.request.get('county_lkf', default_value='all').lower()
        if county_lkf == 'all':
            for lkf in ecore_constants.COUNTY_LKF_CODES.keys():
                _DEFAULT_SOURCING_QUEUE.add(taskqueue.Task(
                    url='/backend/worker/address_registry/source_address_registry_items',
                    params={'county_lkf': lkf, 'filename': filename}
                ))
            return
        else:
            assert county_lkf in ecore_constants.COUNTY_LKF_CODES

        def _dispatch_to_taskqueue(payload):
            while True:
                try:
                    _DEFAULT_QUEUE.add(taskqueue.Task(
                        url='/backend/worker/address_registry/process_address_registry_item',
                        payload=formatting.tq_serialize(payload),
                        countdown=60
                    ))
                    break
                except taskqueue.TaskTooLargeError as e:
                    logging.error('[AddressRegistryHandler.source_address_registry_items] Raised TaskTooLargeError: %r'
                                  % e.message)
                    logging.debug(
                        '[AddressRegistryHandler.source_address_registry_items] len(payload): %i / payload: \'%s\''
                        % (len(formatting.tq_serialize(payload)), pprint.pformat(payload)))
                    return
                except Exception as e:
                    logging.error(
                        '[AddressRegistryHandler.source_address_registry_items] Raised Exception: %r' % e.message)

        cnt = 0
        for payload in ecore_geocoding.AddressRegistryItem.payloads_from_src_file(''.join([_SRC_FILE_BASE_PATH,
                                                                                           filename]), county_lkf):
            _dispatch_to_taskqueue(payload)
            if limit and int(limit) < cnt:  # TODO(mats.blomdahl@gmail.com): Remove debug params.
                break
            else:
                cnt += 1

    # Interface: /backend/worker/address_registry/process_address_registry_item
    def process_address_registry_item(self):
        u"""Writes an address registry item to NDB."""

        def _deprecation_handler(deprecated_date, deprecated_row_item):
            _ADDRESS_REGISTRY_DEPRECATION_QUEUE.add(taskqueue.Task(
                url='/backend/worker/address_registry/process_address_registry_deprecated_item',
                payload=formatting.tq_serialize({
                    'deprecated_date': deprecated_date,
                    'address_registry_row_item': deprecated_row_item
                })
            ))

        task_data = formatting.tq_deserialize(self.request.body)

        entity_key = ecore_geocoding.AddressRegistryItem.update_from_payload(task_data, _deprecation_handler)

        if entity_key:
            logging.info('[AddressRegistryHandler.process_address_registry_item] Wrote entity key \'%s\' to Datastore.'
                         % entity_key.id())

    # Interface: /backend/worker/address_registry/process_address_registry_deprecated_item
    def process_address_registry_deprecated_item(self):
        u"""Writes a deprecated address registry item to NDB.

        Raises:
            NotImplementedError any day of the week.
        """

        task_data = formatting.tq_deserialize(self.request.body)
        logging.debug('[AddressRegistryHandler.process_address_registry_deprecated_item] task_data: \'%s\''
                      % pprint.pformat(task_data))
        raise NotImplementedError('\'process_address_registry_deprecated_item\' not implemented.')

    # Interface: /backend/worker/address_registry/get_from_kwargs?[route_name=<route>[&lkf=<lkf>[&...]]]
    def get_from_kwargs(self):
        u"""Queries the ``AddressRegistryItem`` kind for matching items based on URL params."""

        kwargs = dict([(arg, self.request.get(arg)) for arg in self.request.arguments()])
        self.response.content_type = 'text/plain'
        for result in ecore_geocoding.AddressRegistryItem.get_from_kwargs(**kwargs):
            self.response.write(u'%s\n' % ecore_geocoding.AddressRegistryItem.prettyprint(result))
