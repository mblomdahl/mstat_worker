# -*- coding: utf-8 -*-
#
# Title:   mstat_worker.address_registry
# Author:  Mats Blomdahl
# Version: 2014-02-28

u"""mstat_worker.address_registry module

Usage:
    Invoke URLs at ``/backend/worker/address_registry/<cmd>`` using the GAE TaskQueue or Cron Service.
"""

from address_registry_handler import AddressRegistryHandler
